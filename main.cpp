#include <QtGui>
#include "mainwindow.h"
#include <QTextCodec>
#include <QtWidgets/QApplication>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);

    QTextCodec *wincodec = QTextCodec::codecForName("Windows-1251");
    QTextCodec::setCodecForLocale(wincodec);
    //QTextCodec::setCodecForCStrings(wincodec);

    MainWindow w;
    w.show();
    
    return a.exec();
}
