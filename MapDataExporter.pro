#-------------------------------------------------
#
# Project created by QtCreator 2012-01-25T20:15:44
#
#-------------------------------------------------

QT       += core widgets

TARGET = MapDataExporter
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    mapdata.cpp \
    maplistmodel.cpp \
    regionlistmodel.cpp \
    editdialog.cpp \
    editcolorsdialog.cpp \
    colorpicker/colorpicker.cpp \
    colorpicker/include/iSpawnedObject.cpp \
    colorpicker/qhasmath/spline.cpp \
    colorpicker/qhasmath/QHASPolygon.cpp \
    colorpicker/qhasmath/qhasmath.cpp \
    colorpicker/qhasmath/pigment.cpp \
    colorpicker/qhasmath/matrix3x3.cpp \
    colorpicker/qhasmath/kubelkamunk.cpp \
    colorpicker/qhasmath/gaussianblur.cpp \
    colorpicker/qhasmath/bspline.cpp \
    colorpicker/colorring.cpp \
    colorpicker/colorpair.cpp \
    colorpicker/clickbar.cpp

HEADERS  += mainwindow.h \
    mapdata.h \
    maplistmodel.h \
    regionlistmodel.h \
    editdialog.h \
    editcolorsdialog.h \
    colorpicker/colorpicker.h \
    colorpicker/include/refcounter.h \
    colorpicker/include/qhaswidgets.h \
    colorpicker/include/qhasmath.h \
    colorpicker/include/qhasglobal.h \
    colorpicker/include/loggersignal.h \
    colorpicker/include/iWorkspace.h \
    colorpicker/include/iVerboseObject.h \
    colorpicker/include/iUndostackProxy.h \
    colorpicker/include/iSpawnedObject.h \
    colorpicker/include/iSkin.h \
    colorpicker/include/iScrollarea.h \
    colorpicker/include/iRenderingChain.h \
    colorpicker/include/iPlugin.h \
    colorpicker/include/iPipeline.h \
    colorpicker/include/iPigmentMixture.h \
    colorpicker/include/iPigmentDescription.h \
    colorpicker/include/iPaperSwitch.h \
    colorpicker/include/iPalette.h \
    colorpicker/include/iPaintingStyle.h \
    colorpicker/include/iOpenGLHelper.h \
    colorpicker/include/iLogger.h \
    colorpicker/include/iLayersList.h \
    colorpicker/include/iKeys.h \
    colorpicker/include/iInstrumentProxy.h \
    colorpicker/include/iInstrumentLockEx.h \
    colorpicker/include/iInstrumentLock.h \
    colorpicker/include/iInstrumentList.h \
    colorpicker/include/iInstrument.h \
    colorpicker/include/iImageTransaction.h \
    colorpicker/include/IGravity.h \
    colorpicker/include/iFilter.h \
    colorpicker/include/iDataContainer.h \
    colorpicker/include/iControlsStack.h \
    colorpicker/include/iConnectorRequests.h \
    colorpicker/include/iConnector.h \
    colorpicker/include/iChannel.h \
    colorpicker/include/iCanvasLayer.h \
    colorpicker/include/iCanvas.h \
    colorpicker/include/iArrayGrid.h \
    colorpicker/include/iArrayFilter.h \
    colorpicker/include/iArray.h \
    colorpicker/include/commtypes.h \
    colorpicker/include/commmath.h \
    colorpicker/include/commdefs.h \
    colorpicker/qhasmath/spline.h \
    colorpicker/qhasmath/pigment.h \
    colorpicker/qhasmath/gaussianblur.h \
    colorpicker/colorring.h \
    colorpicker/colorpair.h \
    colorpicker/clickbar.h

FORMS    += mainwindow.ui \
    editdialog.ui \
    editcolorsdialog.ui \
    colorpicker/colorpicker.ui

RESOURCES += \
    mapdataeditor.qrc

RC_FILE = mapdataeditor.rc
