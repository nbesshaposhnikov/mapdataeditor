﻿#include "mapdata.h"

#include <stdio.h>
#include <QObject>
#include <QTextCodec>
#include <limits>

void MapData::decriptData(const unsigned char *enData,int size)
{
    if(data)
        delete data;

    data=new unsigned char[height*width];
    memset(data,255,height*width);

    int Num=0;
    int i,j;
    unsigned char CurData;
    unsigned short CurNum;

    for(i=0;;i+=3)
    {
        if(i>=size) break;
        CurNum=*(unsigned short *)&enData[i];
        CurData=enData[i+2];
        for(j=0;j<CurNum+1;++j)
        {
            data[Num++]=CurData;
        }
    }
}

unsigned char* MapData::criptData(int &size)
{
    QByteArray retData;

    unsigned char prevChar = data[0];

    int i=1;
    unsigned char j;
    while(i<height*width) {
        j=0;

        while(data[i] == prevChar && j < std::numeric_limits<unsigned short>::max()) {
            ++j;
            ++i;
        }
        retData.append((char *)&j,sizeof(unsigned short));
        retData.append(prevChar);
        prevChar = data[i];
        ++i;
    }
    size=retData.size();
    unsigned char *ret = new unsigned char[size];
    memcpy(ret,retData.data(),size);
    return ret;
}

QString from_1251(const char *str)
{
    QByteArray encodedString;
    encodedString.append(str);
    QTextCodec *codec = QTextCodec::codecForName("cp1251");
    QTextCodec::setCodecForLocale(codec);
    QString string = codec->toUnicode(encodedString);

    return string;
}

void MapDataList::load(const QString &fileName)
{
    mapList.clear();

    FILE* dat;
    int mapNum;

    dat=fopen(fileName.toLatin1().data() ,"rb");

    if(dat==NULL)
        return;

    fread(&mapNum,sizeof(int),1,dat);

    int dataEnd,wordSize,regionCount;

    MapData mapData;
    RegionData regData;
    unsigned char *data;
    char str[1024];
    for(int i=0;i<mapNum;++i)
    {
        fread(&mapData.height,sizeof(int),1,dat);
        fread(&mapData.width,sizeof(int),1,dat);
        fread(&wordSize,sizeof(int),1,dat);
        fread(str,wordSize,1,dat);
        mapData.name = from_1251(str);
        fread(&regionCount,sizeof(int),1,dat);
        mapData.regionList.clear();
        for(int j=0;j<regionCount;++j)
        {

            fread(&regData.dwRGB,sizeof(int),1,dat);
            fread(&regData.dwX,sizeof(int),1,dat);
            fread(&regData.dwY,sizeof(int),1,dat);
            fread(&regData.dwSound,sizeof(int),1,dat);
            fread(&wordSize,sizeof(int),1,dat);
            fread(str,wordSize,1,dat);
            regData.name=from_1251(str);

            mapData.regionList.append(regData);
        }


        fread(&dataEnd,sizeof(int),1,dat);
        data=new unsigned char[dataEnd];

        fread(data,dataEnd,1,dat);
        mapData.decriptData(data,dataEnd);
        delete data;

        mapList.append(MapData(mapData));
    }
    fclose(dat);
    return;

}

void to_1251(QString s,char *ascii,unsigned int size)
{
    static QTextCodec *codec=QTextCodec::codecForName("Windows-1251");
    QByteArray ba=codec->fromUnicode(s);
    memcpy(ascii,ba.data(),ba.size());
    ascii[ba.size()]=0;
}

void MapDataList::save(const QString &fileName)
{
    FILE* dat;
    int mapNum = mapList.size();

    dat=fopen(fileName.toLatin1().data() ,"wb");

    if(dat==NULL)
        return;

    fwrite(&mapNum,sizeof(int),1,dat);

    int dataEnd,wordSize,regionCount;

    for(int i=0;i<mapNum;++i)
    {
        fwrite(&mapList[i].height,sizeof(int),1,dat);
        fwrite(&mapList[i].width,sizeof(int),1,dat);
        wordSize = mapList[i].name.size()+1;
        fwrite(&wordSize,sizeof(int),1,dat);
        fwrite(mapList[i].name.toLatin1().data(),wordSize,1,dat);
        regionCount = mapList[i].regionList.size();
        fwrite(&regionCount,sizeof(int),1,dat);
        for(int j=0;j<regionCount;++j)
        {
            fwrite(&mapList[i].regionList[j].dwRGB,sizeof(int),1,dat);
            fwrite(&mapList[i].regionList[j].dwX,sizeof(int),1,dat);
            fwrite(&mapList[i].regionList[j].dwY,sizeof(int),1,dat);
            fwrite(&mapList[i].regionList[j].dwSound,sizeof(int),1,dat);
            wordSize = mapList[i].regionList[j].name.size()+1;
            fwrite(&wordSize,sizeof(int),1,dat);
            char* n = new char[wordSize];
            to_1251(mapList[i].regionList[j].name,n,wordSize);
            fwrite(n,wordSize,1,dat);
            delete n;
        }

        unsigned char *data = mapList[i].criptData(dataEnd);
        fwrite(&dataEnd,sizeof(int),1,dat);
        fwrite(data,dataEnd,1,dat);
        delete data;
    }
    fclose(dat);
    return;
}

