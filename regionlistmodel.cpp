﻿#include "regionlistmodel.h"

RegionListModel::RegionListModel(QObject *parent) :
    QAbstractTableModel(parent),
    mapData(0)
{}

void RegionListModel::setMapData(MapData* newMapData)
{
//    if(mapData == newMapData)
//        return;
    beginResetModel();
    mapData = newMapData;
    endResetModel();
}

int RegionListModel::rowCount ( const QModelIndex & parent) const
{
    return (mapData != 0)?mapData->regionList.size():0;
}

int RegionListModel::columnCount ( const QModelIndex & parent) const
{
    return (mapData != 0)?5:0;
}

QVariant RegionListModel::data ( const QModelIndex & index, int role) const
{
    if (index.row() < 0 || index.column() < 0)
        return QVariant();

    switch (role) {
    case Qt::DisplayRole: return getColumnData(index);break;
    case Qt::EditRole: return getColumnData(index);break;
    case Qt::DecorationRole:
    default:
        return QVariant();
    }
}

bool RegionListModel::setData ( const QModelIndex & index, const QVariant & value, int role)
{
   if (role == Qt::EditRole) {
       return setColumnData(index,value);
   }
   return false;
}

QVariant RegionListModel::getColumnData(const QModelIndex & index) const
{
    switch(index.column()){
    case 0: return index.row()+1; break;
    case 1: return mapData->regionList[index.row()].name; break;
    //case 2: return mapData->regionList[index.row()].dwRGB; break;
    case 2: return mapData->regionList[index.row()].dwX; break;
    case 3: return mapData->regionList[index.row()].dwY; break;
    case 4: return mapData->regionList[index.row()].dwSound; break;
    default: return QVariant();break;
    }
    return QVariant();
}

bool RegionListModel::setColumnData(const QModelIndex & index,const QVariant & value)
{

    switch(index.column()){
    case 0:
          return true;
    case 1: mapData->regionList[index.row()].name = value.toString(); return true;
    //case 2: mapData->regionList[index.row()].dwRGB = value.toInt(); return true;
    case 2: mapData->regionList[index.row()].dwX = value.toInt(); return true;
    case 3: mapData->regionList[index.row()].dwY = value.toInt(); return true;
    case 4: mapData->regionList[index.row()].dwSound = value.toInt(); return true;
    }
    return false;
}

QVariant RegionListModel::headerData ( int section, Qt::Orientation orientation, int role) const
{
    if (role == Qt::DisplayRole && orientation == Qt::Horizontal) {
        switch (section) {
        case 0: return QString(tr("Индекс")); break;
        case 1: return QString(tr("Имя")); break;
       // case 2: return QString(tr("Цвет")); break;
        case 2: return QString(tr("X")); break;
        case 3: return QString(tr("Y")); break;
        case 4: return QString(tr("Музыка")); break;
        }
    }
    return QVariant();
}

Qt::ItemFlags RegionListModel::flags ( const QModelIndex & index ) const
{
    if (!index.isValid())
        return 0;
    if(index.column()==0)
        return Qt::ItemIsEnabled;

    return Qt::ItemIsEnabled | Qt::ItemIsSelectable | Qt::ItemIsEditable;
}

bool RegionListModel::insertRows ( int row, int count, const QModelIndex & parent)
{
//    beginInsertRows(parent,combTable->size(),combTable->size());
//    combTable->insert();
//    endInsertRows();
    return true;
}

bool RegionListModel::removeRows ( int row, int count, const QModelIndex & parent)
{
//    beginRemoveRows(parent,combTable->size()-1,combTable->size()-1);
//    combTable->remove();
//    endRemoveRows();
    return true;
}
