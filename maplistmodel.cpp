#include "maplistmodel.h"

MapListModel::MapListModel(QObject *parent) :
    QAbstractListModel(parent),
    mapList(0)
{
}


void MapListModel::setMapList(MapDataList *newMapList)
{
//    if(mGList == newGList)
//        return;
    beginResetModel();
    mapList = newMapList;
    endResetModel();
}

int MapListModel::rowCount(const QModelIndex &parent) const
{
    return parent.isValid() ? 0 : (mapList ? mapList->mapList.size() : 0);
}

QVariant MapListModel::data(const QModelIndex &index, int role) const
{
    const int mapIndex = index.row();
    if (mapIndex < 0)
        return QVariant();

    switch (role) {
    case Qt::DisplayRole: return mapList->mapList[mapIndex].name;
    case Qt::EditRole:
    case Qt::DecorationRole:
    default:
        return QVariant();
    }
}

bool MapListModel::setData (const QModelIndex & index, const QVariant & value, int role)
{
    if (role == Qt::EditRole) {
        mapList->mapList[index.row()].name = value.toString();
    }
    return false;
}


Qt::ItemFlags MapListModel::flags(const QModelIndex &index) const
{
    if (!index.isValid())
        return 0;

    return Qt::ItemIsEnabled | Qt::ItemIsSelectable | Qt::ItemIsEditable;
}

QVariant MapListModel::headerData(int section, Qt::Orientation orientation,
                                int role) const
{
    if (role == Qt::DisplayRole && orientation == Qt::Horizontal) {
        switch (section) {
        case 0: return tr("Header");
        }
    }
    return QVariant();
}
