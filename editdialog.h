#ifndef EDITDIALOG_H
#define EDITDIALOG_H

#include <QtGui>
#include <QImage>
#include <QPixmap>
#include <QtWidgets/QDialog>
#include <QtWidgets/QGraphicsScene>
#include <QtWidgets/QLabel>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QFileDialog>
#include "mapdata.h"
#include "editcolorsdialog.h"

namespace Ui {
class EditDialog;
}

class EditDialog : public QDialog
{
    Q_OBJECT
    
public:
    explicit EditDialog(const MapData &d,QWidget *parent = 0);
    ~EditDialog();
    MapData &getMapData(){
        return data;
    }
private:
    MapData data;
    Ui::EditDialog *ui;
    QPixmap regionImage;
    QVBoxLayout *colorLayout;
    QGraphicsScene *scene;
    void loadPixmap(QImage image=QImage());
public slots:
    void onImport();
    void onExport();
    void onEdit();
};

#endif // EDITDIALOG_H
