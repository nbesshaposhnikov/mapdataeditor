#ifndef MAPDATA_H
#define MAPDATA_H

#include <QList>
#include <QString>

struct RegionData
{
  QString name;
  unsigned int dwRGB;
  unsigned int dwX;
  unsigned int dwY;
  unsigned int dwSound;
};


class MapData
{
public:
    MapData():
        name(QString()),
        width(0),
        height(0),
        regionList(QList <RegionData> ()),
        data(0)
    {}

    MapData(const QString &n,unsigned int w, unsigned int h,const QList <RegionData> &l,unsigned char *d):
        name(n),
        width(w),
        height(h),
        regionList(l),
        data(d)
    {}

    MapData(const QString &n):
        name(n),
        width(10),
        height(10),
        regionList(QList <RegionData> ()),
        data(new unsigned char[100])
    {
        memset(data,255,100);
    }

    MapData(const MapData &d):
        name(d.name),
        width(d.width),
        height(d.height),
        regionList(d.regionList),
        data(new unsigned char[width*height])

    {memcpy(data,d.data,width*height);}

    MapData &operator = (const MapData &d) {
        name=d.name;
        width = d.width;
        height= d.height;
        regionList = d.regionList;
        if(data)
            delete data;

        data = new unsigned char[width*height];
        memcpy(data,d.data,width*height);

        return *this;
    }

    ~MapData ()
    {
        if(data)
            delete data;
    }
    void decriptData(const unsigned char *enData,int size);
    unsigned char* criptData(int &size);

    void addRegion() {
        regionList.append(RegionData());
    }

    void removeAt(int i) {
        regionList.takeAt(i);
    }

    int findColor(unsigned int RGB) {
        for(int i=0;i<regionList.size();++i) {
            if(regionList[i].dwRGB==RGB)
                return i;
        }
        return regionList.size();
    }

public:
    QString name;
    unsigned int width;
    unsigned int height;
    QList <RegionData> regionList;
    unsigned char *data;

//    unsigned int Count;
//    char *pData;
//    unsigned int DataEnd;
//    char *pRealData;

//    char* Name;
};

class MapDataList {
private:

public:
    QList <MapData> mapList;

    MapDataList():
        mapList(QList <MapData> ())
    {}
    void addMap() {
        mapList.append(MapData("NewMap"));
    }

    void removeAt(int i) {
        mapList.takeAt(i);
    }

    void load(const QString &fileName);
    void save(const QString &fileName);
};

#endif // MAPDATA_H
