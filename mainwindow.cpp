﻿#include "mainwindow.h"
#include "ui_mainwindow.h"

#include <QImage>


MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    fileName = QString();
    ui->setupUi(this);
    ui->centralWidget->setLayout(ui->mainLayout);

   // mapDataList.load("C:/Users/scaR/Desktop/QT_Projects/MapDataExporter-build-desktop-Qt_4_8_0_RC_for_Desktop_-_MSVC2010__Qt_SDK_________/debug/mapdata.dat");
    mapListModel = new MapListModel();
    regionListModel = new RegionListModel();

    ui->mapListView->setModel(mapListModel);
    ui->regionListView->setModel(regionListModel);

    connect(ui->mapListView->selectionModel(),SIGNAL(currentRowChanged(QModelIndex,QModelIndex)),
                             this,SLOT(onMapChange(QModelIndex)));

    connect(ui->addMap,SIGNAL(clicked(bool)),this,SLOT(onAddMap()));
    connect(ui->delMap,SIGNAL(clicked(bool)),this,SLOT(onDeleteMap()));
    connect(ui->addRegion,SIGNAL(clicked(bool)),this,SLOT(onAddRegion()));
    connect(ui->delRegion,SIGNAL(clicked(bool)),this,SLOT(onDeleteRegion()));

    connect(ui->edit,SIGNAL(clicked(bool)),this,SLOT(onEditRegion()));

    connect(ui->open,SIGNAL(triggered()),this,SLOT(onOpen()));
    connect(ui->save,SIGNAL(triggered()),this,SLOT(onSave()));
    connect(ui->saveAs,SIGNAL(triggered()),this,SLOT(onSaveAs()));
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::onMapChange(QModelIndex index)
{
    if(index == QModelIndex())
        return;

    int row = index.row();

    ui->height->setText(QString::number(mapDataList.mapList[row].height));
    ui->width->setText(QString::number(mapDataList.mapList[row].width));

    regionListModel->setMapData(&mapDataList.mapList[row]);
}

void MainWindow::onAddMap()
{
    mapDataList.addMap();
    mapListModel->setMapList(&mapDataList);
    ui->mapListView->setCurrentIndex(mapListModel->index(mapDataList.mapList.size()-1,0));
    regionListModel->setMapData(0);
}

void MainWindow::onDeleteMap()
{
    if(ui->mapListView->currentIndex().row()==-1)
        return;
    mapDataList.removeAt(ui->mapListView->currentIndex().row());

    mapListModel->setMapList(&mapDataList);
    regionListModel->setMapData(0);
}

void MainWindow::onAddRegion()
{
    int row = ui->mapListView->currentIndex().row();

    if(ui->mapListView->currentIndex()==QModelIndex())
        return;

    mapDataList.mapList[row].addRegion();

    regionListModel->setMapData(&mapDataList.mapList[row]);
}

void MainWindow::onDeleteRegion()
{
    int row = ui->mapListView->currentIndex().row();

    if(ui->mapListView->currentIndex()==QModelIndex() || ui->regionListView->currentIndex()==QModelIndex())
        return;

    mapDataList.mapList[row].removeAt(ui->regionListView->currentIndex().row());

    regionListModel->setMapData(&mapDataList.mapList[row]);
}


void MainWindow::onEditRegion()
{
    if(ui->mapListView->currentIndex()==QModelIndex())
        return;

    int map = ui->mapListView->currentIndex().row();

    EditDialog *d = new EditDialog(mapDataList.mapList[map]);

    if(d->exec()) {
        mapDataList.mapList[map] = d->getMapData();
    }

    delete d;
}

void MainWindow::onOpen()
{
    fileName = QFileDialog::getOpenFileName(this,tr("Открыть"),QString(),"MAPDATA (*.dat)");
    if(!fileName.isEmpty())
        mapDataList.load(fileName);

    mapListModel->setMapList(&mapDataList);
}

void MainWindow::onSave()
{
    if(!fileName.isEmpty())
        mapDataList.save(fileName);
}

void MainWindow::onSaveAs()
{
    fileName = QFileDialog::getSaveFileName(this,tr("Сохранить"),QString(),"MAPDATA (*.dat)");
    mapDataList.save(fileName);
}
