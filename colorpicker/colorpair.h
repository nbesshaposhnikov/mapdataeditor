/* Qaquarelle
 * Copyright (C) 2009 Vasiliy Makarov <drmoriarty.0@gmail.com>
 *
 * This is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this software; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#ifndef COLORPAIR_H
#define COLORPAIR_H

#include <QtGui>
#include <QtWidgets/QWidget>

class ColorPair : public QWidget
{
	Q_OBJECT
	Q_PROPERTY(QColor colorOne READ colorOne WRITE setColorOne)
	Q_PROPERTY(QColor colorTwo READ colorTwo WRITE setColorTwo)
		
 public:
	ColorPair(QWidget *parent = 0);
	QColor colorOne() const;
	QColor colorTwo() const;

 public slots:
	void setColorOne(QColor);
	void setColorTwo(QColor);

 signals:
	void colorOneChanged(QColor);
	void colorTwoChanged(QColor);

 protected:
    virtual void paintEvent(QPaintEvent *event);
	virtual void mousePressEvent ( QMouseEvent * event );
	//virtual void mouseMoveEvent ( QMouseEvent * event );

 private:
	QColor cone, ctwo;
};

#endif // COLORPAIR_H
