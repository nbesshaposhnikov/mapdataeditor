/***************************************************************************
 *  Copyright (C) 2009 by Anton R. <commanderkyle@gmail.com>
 *
 *  This file is a part of QAquarelle project.
 *
 *  QAquarelle is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  QAquarelle is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with QAquarelle.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "pigment.h"
#include "../include/commmath.h"
#include "../include/qhasmath.h"
#include <stdio.h>
#include <math.h>
#include <QColor>

/**
 * I use the heuristic approach to generate
 * the KM-parameters, because the promoted one
 * doesn't seem to be stable.
 */
#define KM_SIMPLE


IPigmentDescription * g_fac_SPAWN_PIGMENT_DESCRIPTION (void) {
    return new Pigment ();
}

/// FIXME Need to be adjusted later.
float IPigmentDescription::opacity (ePAINTINGSTYLE_PRESET preset) {
    switch (preset) {
        case PAINTINGSTYLE_PLAIN:
            return 1.00f;
            break;
        case PAINTINGSTYLE_ACRILYCS:
            return 0.50f;
            break;
        case PAINTINGSTYLE_OIL:
            return 1.00f;
            break;
        case PAINTINGSTYLE_INK:
            return 0.95f;
            break;
        case PAINTINGSTYLE_WATERCOLOURS:
        default:
            return 0.30f;
            break;
    }
}

/// FIXME Need to be adjusted later.
float IPigmentDescription::viscosity (ePAINTINGSTYLE_PRESET preset) {
    switch (preset) {
        case PAINTINGSTYLE_PLAIN:
            return 0.00f;
            break;
        case PAINTINGSTYLE_ACRILYCS:
            return 0.50f;
            break;
        case PAINTINGSTYLE_OIL:
            return 0.90f;
            break;
        case PAINTINGSTYLE_INK:
        case PAINTINGSTYLE_WATERCOLOURS:
        default:
            return 0.01f;
            break;
    }
}

/// FIXME Need to be adjusted later.
float IPigmentDescription::dryingspeed (ePAINTINGSTYLE_PRESET preset) {
    switch (preset) {
        case PAINTINGSTYLE_PLAIN:
            return 0.00f;
            break;
        case PAINTINGSTYLE_ACRILYCS:
            return 0.50f;
            break;
        case PAINTINGSTYLE_OIL:
            return 0.05f;
            break;
        case PAINTINGSTYLE_INK:
            return 0.80f;
            break;
        case PAINTINGSTYLE_WATERCOLOURS:
        default:
            return 0.30f;
            break;
    }
}

/// FIXME Need to be adjusted later.
float IPigmentDescription::stainingPower (ePAINTINGSTYLE_PRESET preset) {
    switch (preset) {
        case PAINTINGSTYLE_PLAIN:
            return 0.50f;
            break;
        case PAINTINGSTYLE_ACRILYCS:
            return 0.50f;
            break;
        case PAINTINGSTYLE_OIL:
            return 0.50f;
            break;
        case PAINTINGSTYLE_INK:
            return 1.00f;
            break;
        case PAINTINGSTYLE_WATERCOLOURS:
        default:
            return 0.30f;
            break;
    }
}

Pigment::Pigment(void) {
    m_Id = ISpawnedObject::allocateId ();
    ISpawnedObject::_register (this);

    setName ("Custom");

    m_HasRGBSource  = FALSE;
    m_Granulation   = 0.0f;
    m_Staining      = 0.5f;
    m_Weight        = 0.5f;
    m_Lightfastness = 0.5f;

    m_Absorbing[0] = 0.0f;
    m_Absorbing[1] = 0.0f;
    m_Absorbing[2] = 0.0f;
    m_Absorbing[3] = 0.0f;

    m_Scattering[0] = 0.0f;
    m_Scattering[1] = 0.0f;
    m_Scattering[2] = 0.0f;
    m_Scattering[3] = 0.0f;
}

void Pigment::implicitFromColor (IPaintingStyle * style, const float * rgb) {
    if (rgb)
        implicitFromColor (style, rgb[0], rgb[1], rgb[2]);
}

void Pigment::implicitFromColor (IPaintingStyle * style,
                                 float r, float g, float b) {
    // * Opaque paints, such as Indian Red, exhibit a similar color
    //   on both white and black. Such paints have high scattering in
    //   the same wavelengths as their color, and high absorption in
    //   complementary wavelengths.
    // * Transparent paints, such as Quinacridone Rose, appear colored
    //   on white, and nearly black on black. Such paints have low
    //   scattering in all wavelengths, and high absorption in wavelengths
    //   complementary to their color.
    // * Interference paints, such as Interference Lilac, appear white
    //   (or transparent) and colored on black. Such paints have
    //   high scattering in the same wavelengths as their color, and low
    //   absorption in all wavelengths. Such pigments actually get their
    //   color from interference effects involving the phase of light waves,
    //   which have been modeled accurately by Gondek et al. [11].
    // Cassidy J. Curtis et al. Computer-Generated Watercolor.
    if (style) {
        m_HasRGBSource = TRUE;
        m_RGBSource[0] = r;
        m_RGBSource[1] = g;
        m_RGBSource[2] = b;

#ifdef KM_SIMPLE
        // Simple approach.
        m_Absorbing[0] = MATH::CLAMP (style->paintOpacity () * (1.0f - r), 0.01f, 0.99f);
        m_Absorbing[1] = MATH::CLAMP (style->paintOpacity () * (1.0f - g), 0.01f, 0.99f);
        m_Absorbing[2] = MATH::CLAMP (style->paintOpacity () * (1.0f - b), 0.01f, 0.99f);

        m_Scattering[0] = MATH::CLAMP (style->paintOpacity () * r, 0.01f, 0.99f);
        m_Scattering[1] = MATH::CLAMP (style->paintOpacity () * g, 0.01f, 0.99f);
        m_Scattering[2] = MATH::CLAMP (style->paintOpacity () * b, 0.01f, 0.99f);
#else
        // Approach by Curtis via KM equations inversion.
        const float Rb[3] = {
            MATH::CLAMP (style->paintOpacity () * r, 0.01f, 0.99f),
            MATH::CLAMP (style->paintOpacity () * g, 0.01f, 0.99f),
            MATH::CLAMP (style->paintOpacity () * b, 0.01f, 0.99f)
        };

        const float Rw[3] = {
            MATH::CLAMP (r, 0.01f, 0.99f),
            MATH::CLAMP (g, 0.01f, 0.99f),
            MATH::CLAMP (b, 0.01f, 0.99f)
        };

        const float alpha[3] = {
            0.5f * (Rw[0] + (Rb[0] - Rw[0] + 1.0f) / Rb[0]),
            0.5f * (Rw[1] + (Rb[1] - Rw[1] + 1.0f) / Rb[1]),
            0.5f * (Rw[2] + (Rb[2] - Rw[2] + 1.0f) / Rb[2])
        };
        const float beta[3] = {
            sqrtf (alpha[0] * alpha[0] - 1.0f),
            sqrtf (alpha[1] * alpha[1] - 1.0f),
            sqrtf (alpha[2] * alpha[2] - 1.0f)
        };

        // Compute the values.
        bool mask[3] = {FALSE, FALSE, FALSE};
        float min_a  = 1.0f;
        for (int i = 0; i < 3; i++) {
            const float theta = (beta[i] * beta[i] - (alpha[i] - Rw[i]) *
                                (alpha[i] - 1.0f)) / (beta[i] * (1.0f - Rw[i]));

            if (theta    >= 1.0f &&
                alpha[i] <= 2.0f) {
                m_Scattering[i] = acoth (theta) / beta[i];
                m_Absorbing[i] = m_Scattering[i] * (alpha[i] - 1.0f);
                min_a = MIN (min_a, m_Absorbing[i]);
            } else {
                m_Scattering[i] = 0.01f;
                m_Absorbing[i]  = 0.01f;
                mask[i] = TRUE;
            }
        }

        // Fix some values.
        for (int i = 0; i < 3; i++) {
            if (mask[i])
                m_Absorbing[i] = min_a;
        }

        // Normalize the values.
        m_Scattering[0] /= 10.0f;
        m_Scattering[1] /= 10.0f;
        m_Scattering[2] /= 10.0f;
#endif
    }
}

void Pigment::implicitToColor (IPaintingStyle * style, float * rgb) {
    if (rgb && style) {
        // Just give back the color if we
        // have it saved.
        if (m_HasRGBSource) {
            rgb[0] = m_RGBSource[0];
            rgb[1] = m_RGBSource[1];
            rgb[2] = m_RGBSource[2];
        } else {
            // Otherwise we have to calculate it in some way.
#ifdef KM_SIMPLE
            rgb[0] = MATH::CLAMP_01 (m_Scattering[0] / style->paintOpacity ());
            rgb[1] = MATH::CLAMP_01 (m_Scattering[1] / style->paintOpacity ());
            rgb[2] = MATH::CLAMP_01 (m_Scattering[2] / style->paintOpacity ());
#else
#   warning("implicitToColor() is not implemented for this mode yet.");
#endif
        }
    }
}

void Pigment::setAbsorbing  (const TCC * a) {
    if (a) {
        m_Absorbing[0] = a[0];
        m_Absorbing[1] = a[1];
        m_Absorbing[2] = a[2];

        m_HasRGBSource  = FALSE;
    }
}

void Pigment::setScattering (const TCC * s) {
    if (s) {
        m_Scattering[0] = s[0];
        m_Scattering[1] = s[1];
        m_Scattering[2] = s[2];

        m_HasRGBSource  = FALSE;
    }
}

long Pigment::toXML (char * buffer, int capacity) const {
    long written = 0L;
    written += snprintf (buffer + written, capacity - written, "<pigment name=\"%s\">\n\t", m_Name);
    written += snprintf (buffer + written, capacity - written, "<physical granulation=\"%f\" density=\"%f\" staining_power=\"%f\" lightfastness=\"%f\" />\n\t",
                         m_Granulation, m_Weight, m_Staining, 0.5f);
    written += snprintf (buffer + written, capacity - written, "<optical>\n\t\t<absorbing r=\"%f\" g=\"%f\" b=\"%f\" />\n\t\t",
                         m_Absorbing[0], m_Absorbing[1], m_Absorbing[2]);
    written += snprintf (buffer + written, capacity - written, "<scattering r=\"%f\" g=\"%f\" b=\"%f\" />\n\t",
                         m_Scattering[0], m_Scattering[1], m_Scattering[2]);
    written += snprintf (buffer + written, capacity - written, "</optical>\n</pigment>\n");

    return written;
}

int Pigment::sortIndexHue (void) const {
    return QColor::fromRgbF (m_Scattering[0],
                             m_Scattering[1],
                             m_Scattering[2]).hue ();
}

int Pigment::sortIndexBrightness (void) const {
    return QColor::fromRgbF (m_Scattering[0],
                             m_Scattering[1],
                             m_Scattering[2]).value ();
}
