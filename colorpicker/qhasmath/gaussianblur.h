/***************************************************************************
 *   Copyright (C) 2009 by Anton R. <commanderkyle@gmail.com>              *
 *                                                                         *
 *   This file is a part of QAquarelle project.                            *
 *                                                                         *
 *   QAquarelle is free software; you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/
#ifndef GAUSSIANBLUR_H
#define GAUSSIANBLUR_H

#include "../include/commdefs.h"
#include "../include/commtypes.h"
#include "../include/refcounter.h"

#include "../include/iArray.h"
#include "../include/iArrayFilter.h"


class GaussianBlur : public IArrayFilter,
                     public CRefCounter {

    int      m_Id;
    int      m_KernelSize;
    float    m_Radius;
    float    m_KernelSum;
    float *  m_Kernel;

    virtual void suicide (void) {
        delete this;
    }

    GaussianBlur (void);
    virtual ~GaussianBlur (void);

protected:

    void rebuildFilter (float r);
    void clear (void);

friend class QHASMath;
public:

    /**
     * Blur the given target with
     * specified radius.
     * @param src Source/destination image.
     * @param tmp Temporary image buffer.
     * @param r Blur radius in pixels.
     */
    virtual void filter (IArray * src, IArray * tmp, float r);

    /******** ISPAWNEDOBJECT IMPLANTS **************/

    /**
     * Get the classname for this object.
     */
    virtual const char * myClassName (void) {
        return "Common::GaussianBlur";
    }

    /**
     * Get the Id of current allocated object.
     */
    virtual int objectId (void) {
        return m_Id;
    }

    /**
     * Increase the object's
     * reference counter.
     */
    virtual void addRef (void) {
        CRefCounter::addRef ();
    }

    /**
     * Release the object.
     */
    virtual void release (void) {
        CRefCounter::release ();
    }

    /**
     * Ask the object to emit all
     * notification signals if any.
     */
    virtual void pokeObject (void) {
    }
};

#endif // GAUSSIANBLUR_H
