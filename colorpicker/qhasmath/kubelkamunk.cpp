/***************************************************************************
 *   Copyright (C) 2009 by Anton R. <commanderkyle@gmail.com>              *
 *                                                                         *
 *   This file is a part of QAquarelle project.                            *
 *                                                                         *
 *   QAquarelle is free software; you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/
#include "../include/qhasmath.h"
#include "../include/commmath.h"
#include <math.h>

/// Kubelka-Munk implementation to display
/// some stuff in GUI.

static const float g_ONES[4] = {1.0f, 1.0f, 1.0f, 1.0f};
static const float g_ZERO[4] = {0.0f, 0.0f, 0.0f, 0.0f};


void KubelkaMunk::colorOverBackground (
            const float * a,
            const float * s,
            const float * Rbg,
                  float * rgb
) {
    // Some precomputations...
    // (channels are premultiplied)
    const float alpha[4] = {
        (2.0f * a[0] / 20.0f + s[0]) / s[0],
        (2.0f * a[1] / 20.0f + s[1]) / s[1],
        (2.0f * a[2] / 20.0f + s[2]) / s[2],
        1.0f
    };
    const float beta[4] = {
        sqrtf (alpha[0] * alpha[0] - 1.0f),
        sqrtf (alpha[1] * alpha[1] - 1.0f),
        sqrtf (alpha[2] * alpha[2] - 1.0f),
        sqrtf (alpha[3] * alpha[3] - 1.0f)
    };
    const float theta[4] = {
        1.0f * 20.0f * s[0] * beta[0],
        1.0f * 20.0f * s[1] * beta[1],
        1.0f * 20.0f * s[2] * beta[2],
        1.0f * 20.0f * s[3] * beta[3]
    };

    // Some scary hyperbolic functions...
    const float sih[4] = {
        0.5f * (expf (+theta[0]) - expf (-theta[0])),
        0.5f * (expf (+theta[1]) - expf (-theta[1])),
        0.5f * (expf (+theta[2]) - expf (-theta[2])),
        0.5f * (expf (+theta[3]) - expf (-theta[3]))
    };
    const float coh[4] = {
        0.5f * (expf (+theta[0]) + expf (-theta[0])),
        0.5f * (expf (+theta[1]) + expf (-theta[1])),
        0.5f * (expf (+theta[2]) + expf (-theta[2])),
        0.5f * (expf (+theta[3]) + expf (-theta[3]))
    };
    const float denum[4] = {
        alpha[0] * sih[0] + beta[0] * coh[0],
        alpha[1] * sih[1] + beta[1] * coh[1],
        alpha[2] * sih[2] + beta[2] * coh[2],
        alpha[3] * sih[3] + beta[3] * coh[3]
    };

    // Layer reflectance and transmittance.
    const float R[4] = {
        sih[0] / denum[0],
        sih[1] / denum[1],
        sih[2] / denum[2],
        sih[3] / denum[3]
    };
    const float T[4] = {
        beta[0] / denum[0],
        beta[1] / denum[1],
        beta[2] / denum[2],
        beta[3] / denum[3]
    };

    // Integrate with the background.
    const float R1[4] = {
        R[0] + T[0] * T[0] * Rbg[0] / (1.0f - R[0] * Rbg[0]),
        R[1] + T[1] * T[1] * Rbg[1] / (1.0f - R[1] * Rbg[1]),
        R[2] + T[2] * T[2] * Rbg[2] / (1.0f - R[2] * Rbg[2]),
        R[3] + T[3] * T[3] * Rbg[3] / (1.0f - R[3] * Rbg[3])
    };

    // Resulitng color.
    rgb[0] = MATH::CLAMP_01 (R1[0]);
    rgb[1] = MATH::CLAMP_01 (R1[1]);
    rgb[2] = MATH::CLAMP_01 (R1[2]);
}

void KubelkaMunk::colorOverWhite (
            const float * absorbing,
            const float * scattering,
                  float * rgb
) {
    colorOverBackground (absorbing, scattering, g_ONES, rgb);
}

void KubelkaMunk::colorOverBlack (
            const float * absorbing,
            const float * scattering,
                  float * rgb
) {
    colorOverBackground (absorbing, scattering, g_ZERO, rgb);
}
