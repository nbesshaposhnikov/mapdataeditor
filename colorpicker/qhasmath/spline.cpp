/***
 *  Copyright (C) 2009 by Valeriy Fedotov
 *
 *  This file is a part of QAquarelle project.
 *
 *  QAquarelle is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  QAquarelle is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with QAquarelle.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
#include "spline.h"
#include "../include/commdefs.h"

#include <stdlib.h>

ISpline * QHASMath::newSpline (const double *x, const double *f, int nPoints) {
    return new Spline (x, f, nPoints);
}

// Note: For details of algorithm of Natural Cubic slpine interpolation see
// http://en.wikipedia.org/wiki/Spline_(mathematics)#Algorithm_for_computing_natural_cubic_splines

// Function for sorting spline points by x axis.
int cmpSplineCoefs(const void *c1, const void *c2){
    double x1 = ((SplineCoefs *)c1)->x;
    double x2 = ((SplineCoefs *)c2)->x;
    if( x1 < x2 )  return -1;
    if( x2 == x2 ) return 0;
    return 1;
}

// Init function, is called from constructors after populatin
// x[i] and a[i] spline coefficients.
void Spline::init()
{
    qsort(m_coefs, m_n + 1, sizeof(SplineCoefs), cmpSplineCoefs);
    int i;
    double h[m_n];
    for(i = 0; i < m_n; i++){
        h[i] = m_coefs[i+1].x - m_coefs[i].x;
    }
    double alpha[m_n];
    for(i = 1; i < m_n; i++){
        alpha[i] = 3 / h[i] * (m_coefs[i+1].a - m_coefs[i].a) - 3 / h[i-1] * (m_coefs[i].a - m_coefs[i-1].a);
    }
    double l[m_n+1], mu[m_n+1], z[m_n+1];
    l[0] = 1;
    mu[0] = z[0] = 0;
    for(i = 1; i < m_n; i++){
        l[i] = 2.0 * (m_coefs[i+1].x - m_coefs[i-1].x) - h[i-1] * mu[i-1];
        mu[i] = h[i] / l[i];
        z[i] = (alpha[i] - h[i-1] * z[i-1]) / l[i];
    }
    l[m_n] = 1;
    z[m_n] = m_coefs[m_n].c = 0;
    for(int j = m_n-1; j >= 0; j--){
        m_coefs[j].c = z[j] - mu[j] * m_coefs[j+1].c;
        m_coefs[j].b =  (m_coefs[j+1].a - m_coefs[j].a) / h[j] - h[j] * ( m_coefs[j+1].c + 2.0 * m_coefs[j].c ) / 3.0;
        m_coefs[j].d = (m_coefs[j+1].c - m_coefs[j].c) / (3.0 * h[j]);
    }
}

Spline::Spline(QList <QPointF> &points)
{
    m_coefs = NULL;
    rebuild (points);
}

Spline::Spline(const double *x, const double *f, int nPoints)
{
    m_coefs = NULL;
    rebuild (x, f, nPoints);
}

void Spline::rebuild (const double *x, const double *f, int nPoints) {
    // [castedflake] Avoid the reallocation if we have the array of sufficient length.
    if (m_n < nPoints || m_coefs == NULL) {
        SAFE_DELETE_A (m_coefs);

        m_n = nPoints - 1;
        m_coefs = new SplineCoefs[m_n + 1];
    } else {
        m_n = nPoints - 1;
    }

    for(int i = 0; i <= m_n; i++){
        m_coefs[i].x = x[i];
        m_coefs[i].a = f[i];
    }

    init();
}

void Spline::rebuild (QList <QPointF> &points) {
    // [castedflake] Avoid the reallocation if we have the array of sufficient length.
    if (m_n < points.size() || m_coefs == NULL) {
        SAFE_DELETE_A (m_coefs);

        m_n = points.size() - 1;
        m_coefs = new SplineCoefs[m_n + 1];
    } else {
        m_n = points.size() - 1;
    }

    for(int i = 0; i <= m_n; i++){
        m_coefs[i].x = points[i].x();
        m_coefs[i].a = points[i].y();
    }

    init();
}

double Spline::f(double x) const {
    int i;
    for(i = 0; i < m_n; i++){
        if(x < m_coefs[i+1].x)
            break;
    }
    double dx = x - m_coefs[i].x;
    // Return with Horner scheme.
    return m_coefs[i].a + dx * ( m_coefs[i].b + dx * ( m_coefs[i].c + m_coefs[i].d * dx) );
}

Spline::~Spline()
{
    SAFE_DELETE_A (m_coefs);
}
