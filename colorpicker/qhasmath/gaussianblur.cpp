/***************************************************************************
 *  Copyright (C) 2009 by Anton R. <commanderkyle@gmail.com>
 *
 *  This file is a part of QAquarelle project.
 *
 *  QAquarelle is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  QAquarelle is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with QAquarelle.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "gaussianblur.h"
#include <math.h>

GaussianBlur::GaussianBlur (void) {
    m_Radius     = 0.0f;
    m_Kernel     = NULL;
    m_KernelSize = 0;
    m_KernelSum  = 0.0f;

    m_Id = ISpawnedObject::allocateId ();
    ISpawnedObject::_register (this);
}

GaussianBlur::~GaussianBlur (void) {
    ISpawnedObject::_unregister (this);
    clear ();
}

void GaussianBlur::clear (void) {
    SAFE_DELETE_A (m_Kernel);
    m_KernelSize = 0;
    m_KernelSum  = 0.0f;
}

void GaussianBlur::rebuildFilter (float r) {
    clear ();

    const int radius = (int) r;
    const int size   = 2 * radius + 1;

    m_KernelSize = size;

    // Create the new buffers.
    m_Kernel    = new float[size];
    m_KernelSum = 0.0f;
    for (int i = 1; i <= radius; i++) {
        const int szi = radius - i;
        const int szj = radius + i;

        m_Kernel[szj] = (szi + 1) * (szi + 1);
        m_Kernel[szi] = m_Kernel[szj];
        m_KernelSum  += (m_Kernel[szi] + m_Kernel[szj]);
    }

    m_Kernel[radius] = (radius + 1) * (radius + 1);
    m_KernelSum += m_Kernel[radius];
}

void GaussianBlur::filter (IArray * src, IArray * tmp, float r) {
    // Make sure we have the valid
    // parameters.
    if (!src || !tmp || r < 1.0f)
        return;

    if (tmp->width  () != src->width () ||
        tmp->height () != src->height ())
        return;

    // Make sure we have a valid filter.
    if (fabsf (m_Radius - r) > 0.1f ||
        m_Kernel == NULL) {
        rebuildFilter (r);
    }

    const int radius = (int) r;
    const int size   = 2 * radius + 1;
    const int wstep  = src->width ();
    const int hstep  = src->height ();

    // Lock the required data.
    float * p_src = (float*) src->lock_pointer ();
    float * p_tmp = (float*) tmp->lock_pointer ();

    // Horizontal blur.
    int line = 0;
    for (int i = 0; i < hstep; i++) {
        for (int j = 0; j < wstep; j++) {
            float gsum = 0;
            float sum  = 0;

            // Circular index.
            int read = (wstep + j - radius) % wstep;
            for (int z = 0; z < size; z++) {
                gsum += m_Kernel[z] * p_src[line + read];
                sum  += m_Kernel[z];

                read = (read + 1) % wstep;
            }

            p_tmp[line + j] = gsum / sum;
        }

        line += wstep;
    }

    // Vertical blur.
    line = 0;
    for (int i = 0; i < hstep; i++) {
        for (int j = 0; j < wstep; j++) {
            float gsum = 0;
            float sum  = 0;

            // Circular index.
            int read = (hstep + i - radius) % hstep;
            for (int z = 0; z < size; z++) {
                gsum += m_Kernel[z] * p_tmp[read * wstep + j];
                sum  += m_Kernel[z];

                read = (read + 1) % hstep;
            }

            p_src[line + j] = gsum / sum;
        }

        line += wstep;
    }

    // Unlock.
    src->unlock ();
    tmp->unlock ();
}
