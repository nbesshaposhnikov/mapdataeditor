/***************************************************************************
 *   Copyright (C) 2009 by Anton R. <commanderkyle@gmail.com>              *
 *                                                                         *
 *   This file is a part of QAquarelle project.                            *
 *                                                                         *
 *   QAquarelle is free software; you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/
#include "../include/qhasmath.h"

void g_MAT3x3_identity (float * m) {
    m[0] = 1.0f;
    m[1] = 0.0f;
    m[2] = 0.0f;
    m[3] = 0.0f;
    m[4] = 1.0f;
    m[5] = 0.0f;
    m[6] = 0.0f;
    m[7] = 0.0f;
    m[8] = 1.0f;
}

void g_MAT3x3_vmultiply (const float * v0, const float * m, float * v) {
    v[0] = v0[0] * m[0] + v0[1] * m[3] + v0[2] * m[6];
    v[1] = v0[0] * m[1] + v0[1] * m[4] + v0[2] * m[7];
    v[2] = v0[0] * m[2] + v0[1] * m[5] + v0[2] * m[8];
}

void g_MAT3x3_mmultiply (const float * m0, const float * m1, float * m) {
    m[0] = m0[0] * m1[0] + m0[1] * m1[3] + m0[2] * m1[6];
    m[1] = m0[0] * m1[1] + m0[1] * m1[4] + m0[2] * m1[7];
    m[2] = m0[0] * m1[2] + m0[1] * m1[5] + m0[2] * m1[8];
    m[3] = m0[3] * m1[0] + m0[4] * m1[3] + m0[5] * m1[6];
    m[4] = m0[3] * m1[1] + m0[4] * m1[4] + m0[5] * m1[7];
    m[5] = m0[3] * m1[2] + m0[4] * m1[5] + m0[5] * m1[8];
    m[6] = m0[6] * m1[0] + m0[7] * m1[3] + m0[8] * m1[6];
    m[7] = m0[6] * m1[1] + m0[7] * m1[4] + m0[8] * m1[7];
    m[8] = m0[6] * m1[2] + m0[7] * m1[5] + m0[8] * m1[8];
}

void g_MAT3x3_invert (const float * m0, float * m1) {

}

