/***************************************************************************
 *   Copyright (C) 2009 by Anton R. <commanderkyle@gmail.com>              *
 *                                                                         *
 *   This file is a part of QAquarelle project.                            *
 *                                                                         *
 *   QAquarelle is free software; you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/
#ifndef PIGMENT_H
#define PIGMENT_H

#include "../include/iPigmentDescription.h"
#include <string.h>


class Pigment : public IPigmentDescription,
                public CRefCounter {
public:

    /****** IPIGMENTDESCRIPTION IMPLANTS ********/

    /**
     * Generate the extended color description
     * based on the given painting style.
     */
    virtual void implicitFromColor (IPaintingStyle * style, const float * rgb);

    /**
     * Generate the extended color description
     * based on the given painting style descriptor.
     */
    virtual void implicitFromColor (IPaintingStyle * style,
                                    float r, float g, float b);

    /**
     * Convert the extended color description
     * to RGB data using the given painting style.
     */
    virtual void implicitToColor (IPaintingStyle * style, float * rgb);


    virtual void setAbsorbing  (const TCC * a);
    virtual void setScattering (const TCC * s);

    virtual void setName (const char * name) {
        strncpy (m_Name, name, 128);
    }

    virtual void setWeight (float v) {
        m_Weight = v;
    }

    virtual void setGranulation (float v) {
        m_Granulation = v;
    }

    virtual void setStainingPower (float v) {
        m_Staining = v;
    }

    virtual void setLightfastness (float v) {
        m_Lightfastness = v;
    }

    /********** IPIGMENTMIXTURE IMPLANTS ***********/

    /**
     * Pigment name if any.
     */
    virtual const char * name (void) const {
        return m_Name;
    }

    virtual float granulation (void) const {
        return m_Granulation;
    }

    virtual float stainingPower (void) const {
        return m_Staining;
    }

    virtual float weight (void) const {
        return m_Weight;
    }

    /**
     * Returns the pointer to an RGB array
     * of absorbtion coefficients.
     */
    virtual const TCC * absorbing (void) const {
        return m_Absorbing;
    }

    /**
     * Returns the pointer to an RGB array of
     * scattering coefficients.
     */
    virtual const TCC * scattering (void) const {
        return m_Scattering;
    }

    /**
     * Write the XML description into
     * the buffer and returns the number
     * of bytes written.
     */
    virtual long toXML (char * buffer, int capacity) const;

    /**
     * Estimated color hue.
     */
    virtual int sortIndexHue (void) const;

    /**
     * Estimated color brightness.
     */
    virtual int sortIndexBrightness (void) const;

    /**
     * Check if the color description is valid.
     */
    virtual bool isValid (void) const {
        return ((m_Absorbing[0]  >= 0.01f && m_Absorbing[0]  <= 1.00f) ||
                (m_Absorbing[1]  >= 0.01f && m_Absorbing[1]  <= 1.00f) ||
                (m_Absorbing[2]  >= 0.01f && m_Absorbing[2]  <= 1.00f)) &&
               ((m_Scattering[0] >= 0.01f && m_Scattering[0] <= 1.00f) ||
                (m_Scattering[1] >= 0.01f && m_Scattering[1] <= 1.00f) ||
                (m_Scattering[2] >= 0.01f && m_Scattering[2] <= 1.00f));
    }

    /******** ISPAWNEDOBJECT IMPLANTS **********/

    /**
     * Get the classname for this object.
     */
    virtual const char * myClassName (void) {
        return "Common::PigmentDescriptor";
    }

    /**
     * Get the Id of current allocated object.
     */
    virtual int objectId (void) {
        return m_Id;
    }

    /**
     * Increase the object's
     * reference counter.
     */
    virtual void addRef (void) {
        CRefCounter::addRef ();
    }

    /**
     * Release the object.
     */
    virtual void release (void) {
        CRefCounter::release ();
    }

    /**
     * Ask the object to emit all
     * notification signals if any.
     */
    virtual void pokeObject (void) {
    }

protected:
friend IPigmentDescription * g_fac_SPAWN_PIGMENT_DESCRIPTION (void);

    int     m_Id;
    float   m_Granulation;
    float   m_Staining;
    float   m_Weight;
    float   m_Lightfastness;
    float   m_Absorbing[4]  __attribute__((aligned(16)));
    float   m_Scattering[4] __attribute__((aligned(16)));
    float   m_RGBSource[4]  __attribute__((aligned(16)));
    bool    m_HasRGBSource;

    char    m_Name[128];

    Pigment (void);
    virtual ~Pigment (void) {
        ISpawnedObject::_unregister (this);
    }

    virtual void suicide (void) {
        delete this;
    }
};

#endif // PIGMENT_H
