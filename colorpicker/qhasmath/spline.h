/***
 *  Copyright (C) 2009 by Valeriy Fedotov
 *
 *  This file is a part of QAquarelle project.
 *
 *  QAquarelle is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  QAquarelle is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with QAquarelle.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
#ifndef SPLINE_H
#define SPLINE_H

#include <QList>
#include <QPointF>
#include "../include/qhasmath.h"

// Spline coefficients.
struct SplineCoefs
{
    double a;
    double b;
    double c;
    double d;
    double x;
};

// Cubic spline class.
class Spline : public ISpline
{
public:
    Spline(QList <QPointF> &points);
    Spline(const double *x, const double *f, int nPoints);
    virtual ~Spline();

    virtual double f(double x) const;

    virtual void release (void) {
        delete this;
    }

    /**
     * Replace the points and rebuild the spline.
     */
    virtual void rebuild (const double *x, const double *f, int nPoints);
    virtual void rebuild (QList <QPointF> &points);

private:
    void init();  // Constructor helper.

    int m_n;
    SplineCoefs *m_coefs;
};

#endif // SPLINE_H
