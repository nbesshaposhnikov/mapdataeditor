/***************************************************************************
 *   Copyright (C) 2009 by Anton R. <commanderkyle@gmail.com>              *
 *                                                                         *
 *   This file is a part of QAquarelle project.                            *
 *                                                                         *
 *   QAquarelle is free software; you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/
#include "../include/qhasmath.h"
#include <memory.h>

bool QHASPolygon::intersects (const sRect * rect0, const sRect * rect1, sRect * dest) {
    bool result = (rect1->left   < rect0->right &&
                   rect1->right  > rect0->left &&
                   rect1->top    < rect0->bottom &&
                   rect1->bottom > rect0->top);

    if (result && dest) {
        dest->left   = MIN (rect0->left,   rect1->left);
        dest->top    = MIN (rect0->top,    rect1->top);
        dest->right  = MAX (rect0->right,  rect1->right);
        dest->bottom = MAX (rect0->bottom, rect1->bottom);
    }

    return result;
}

int QHASPolygon::convexHull (sPoint3f ** list, int length) {
    if (length > 3) {
        // Find a center.
        float cx = 0.0f;
        float cy = 0.0f;
        float cz = 0.0f;
        for (int i = 0; i < length; i++) {
            cx += list[i]->x ();
            cy += list[i]->y ();
            cz += list[i]->z ();
        }

        cx /= length;
        cy /= length;
        cz /= length;

        sPoint3f center (cx, cy, cz);

        // Sort the vertices by their
        // distance from center.
        // (selection sort)
        for (int i = 0; i < length; i++) {
            int   idx  = i;
            float best = list[i]->distanceTo (&center);
            for (int j = i + 1; j < length; j++) {
                const float d = list[j]->distanceTo (&center);
                if (best < d) {
                    best = d;
                    idx  = j;
                }
            }

            SWAP (list[i], list[idx]);
        }

        // FIXME possible buffer overrun.
        bool mask[128];
        memset (mask, 0, sizeof(bool) * 128);

        // Enumerate all possible triangles.
        for (int i = 0; i < length; i++) {
            if (mask[i]) continue;
                mask[i] = TRUE;
            for (int j = i + 1; j < length; j++) {
                if (mask[j]) continue;
                    mask[j] = TRUE;
                for (int k = j + 1; k < length; k++) {
                    if (mask[k]) continue;
                        mask[k] = TRUE;

                    // Remove all points, that are inside
                    // the triangle.
                    for (int l = 0; l < length; l++) {
                        if (!mask[l]) {
                             mask[l] = list[l]->inTriangle_xz (list[i], list[j], list[k]);
                        }
                    }

                    mask[k] = FALSE;
                }
                mask[j] = FALSE;
            }
            mask[i] = FALSE;
        }

        // Remove all masked points from the list.
        int recounted = 0;
        for (int i = 0; i < length; i++) {
            if (!mask[i])
                list[recounted++] = list[i];
        }

        // Sort the points by angle.
        int angles[256];
        for (int i = 0; i < recounted; i++) {
            angles[i] = g_angleFromCoords (list[i]->x (), list[i]->z ());
        }

        for (int i = 0; i < recounted - 1; i++) {
            int idx = i;
            for (int j = i + 1; j < recounted; j++) {
                if (angles[j] < angles[idx])
                    idx = j;
            }

            SWAP (list[i],   list[idx]);
            SWAP (angles[i], angles[idx]);
        }

        return recounted;
    } else
        return length;
}
