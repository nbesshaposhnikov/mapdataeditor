#include "../include/qhasmath.h"
#include "gaussianblur.h"
#include "../include/commmath.h"
#include <math.h>
#include <stdlib.h>
#include <assert.h>

float inline sqr (float x) {
    return x * x;
}

float atanh (float x) {
    return 0.5f * log ((1.0f + x) / (1.0f - x));
}

float acoth (float x) {
    assert (x >= 1);
    return logf (x + sqrtf (x * x - 1.0f));
}

float sPoint3f::distanceTo (const sPoint3f * target) const {
    return sqrtf (distance2To (target));
}

float sPoint3f::distance2To (const sPoint3f * target) const {
    return sqr (x () - target->x ()) + sqr (y () - target->y ()) + sqr (z () - target->z ());
}

bool sPoint3f::inTriangle_xz (const sPoint3f * a, const sPoint3f * b, const sPoint3f * c) const {
    const float b0 = (b->x () - a->x ()) * (c->z () - a->z ()) - (c->x () - a->x ()) * (b->z () - a->z ());
    if (fabsf (b0) <= 0.001f)
        return FALSE;

    const float b1 = ((b->x () - x ()) * (c->z () - z ()) - (c->x () - x ()) * (b->z () - z ())) / b0;
    const float b2 = ((c->x () - x ()) * (a->z () - z ()) - (a->x () - x ()) * (c->z () - z ())) / b0;
    const float b3 = ((a->x () - x ()) * (b->z () - z ()) - (b->x () - x ()) * (a->z () - z ())) / b0;

    return b1 >= 0.0f &&
           b2 >= 0.0f &&
           b3 >= 0.0f;
}

float sPoint2f::distanceTo (const sPoint2f * target) const {
    return sqrtf (distance2To (target));
}

float sPoint2f::distance2To (const sPoint2f * target) const {
    return sqr (x () - target->x ()) + sqr (y () - target->y ());
}


IArrayFilter * QHASMath::newGaussianBlur (void) {
    return new GaussianBlur ();
}

/// Box-Muller transform.
float QHASMath::randGaussian (void) {
    float x1, x2;
    float w;
    do {
         x1 = (2.0f * rand ()) / RAND_MAX - 1.0f;
         x2 = (2.0f * rand ()) / RAND_MAX - 1.0f;
         w = x1 * x1 + x2 * x2;
    } while (w >= 1.0f);

    w = sqrtf ((-2.0f * logf (w)) / w);
    return x1 * w;
    //y1 = x1 * w;
    //y2 = x2 * w;
}

int g_angleFromCoords (float x, float y) {
    return (int)(180.0f * atan2f (y, x) / M_PI);
}

float smoothStep (float edge0, float edge1, float x) {
    const float x1 = MATH::CLAMP_01 ((x - edge0) / (edge1 - edge0));
    return x1 * x1 * (3.0f - 2.0f * x1);
}
