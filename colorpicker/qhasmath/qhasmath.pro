# -------------------------------------------------
# Project created by QtCreator 2009-01-25T10:54:51
# -------------------------------------------------
include(../../../qmakeroot.pri)

DEFINES -= QT_OPENGL_LIB

SOURCES += qhasmath.cpp \
    gaussianblur.cpp \
    matrix3x3.cpp \
    bspline.cpp \
    kubelkamunk.cpp \
    pigment.cpp \
    QHASPolygon.cpp \
    spline.cpp
HEADERS += gaussianblur.h \
    pigment.h \
    spline.h

DESTDIR = ../../../lib/
TARGET = qhasmath
TEMPLATE = lib
CONFIG += staticlib

OBJECTS_DIR = ../../../obj
MOC_DIR = ../../../obj
RCC_DIR = ../../../obj
UI_DIR = .

TRANSLATIONS = ../../../ru.ts

QMAKE_CXXFLAGS += -O3 -fmerge-constants -ffinite-math-only

