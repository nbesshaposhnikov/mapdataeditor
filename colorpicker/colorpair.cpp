/* Qaquarelle
 * Copyright (C) 2009 Vasiliy Makarov <drmoriarty.0@gmail.com>
 *
 * This is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this software; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#include "colorpair.h"

ColorPair::ColorPair(QWidget *parent) : QWidget(parent)
{
	cone = QColor("white");
	ctwo = QColor("black");
}

QColor ColorPair::colorOne() const
{
	return cone;
}

QColor ColorPair::colorTwo() const
{
	return ctwo;
}

void ColorPair::setColorOne(QColor c)
{
	if(cone != c) {
		cone = c;
		emit colorOneChanged(cone);
		repaint();
	}
}

void ColorPair::setColorTwo(QColor c)
{
	if(ctwo != c) {
		ctwo = c;
		emit colorTwoChanged(ctwo);
		repaint();
	}
}

void ColorPair::paintEvent(QPaintEvent *event)
{
	const float d = 0.7f;
	QPainter painter(this);
	QRect r = contentsRect(), r2 = r;
        r.moveTo ((int)(r.width()*(1.f-d)), (int)(r.height()*(1.f-d)));
        r.setWidth ((int)(r.width()*d));
        r.setHeight ((int)(r.height()*d));
        r2.setWidth ((int)(r.width()));
        r2.setHeight ((int)(r.height()));
	painter.setBrush(ctwo);
	painter.drawRect(r2);
	painter.setBrush(cone);
	painter.drawRect(r);
}

void ColorPair::mousePressEvent ( QMouseEvent * event )
{
	QColor c = cone;
	cone = ctwo;
	ctwo = c;
	emit colorOneChanged(cone);
	emit colorTwoChanged(ctwo);
	repaint();
}
