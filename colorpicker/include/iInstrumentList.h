/***************************************************************************
 *   Copyright (C) 2009 by Anton R. <commanderkyle@gmail.com>              *
 *                                                                         *
 *   This file is a part of QAquarelle project.                            *
 *                                                                         *
 *   QAquarelle is free software; you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/
#ifndef IINSTRUMENTLIST_H
#define IINSTRUMENTLIST_H

#include "iVerboseObject.h"
#include "iSpawnedObject.h"
#include "iInstrumentProxy.h"
#include "iInstrument.h"
#include "iConnector.h"


class IInstrumentList : public IVerboseObject,
                        public IConnectorCallback {
// Public stuff
public:

    virtual QWidget * asWidget (void) = 0;

    /**
     * Add an instrument to
     * the end of the list.
     */
    virtual void append (IInstrument * instrument) = 0;

    /**
     * Add an instrument to
     * the beginning of the list.
     */
    virtual void prepend (IInstrument * instrument) = 0;

    /**
     * Remove an instrument at
     * given position.
     */
    virtual void removeAt (int index) = 0;

    /**
     * Remove the specified instrument
     * from list.
     */
    virtual void remove (IInstrument * target) = 0;

    /**
     * Returns the instrument at given position
     * or NULL if position is illegal ar empty.
     */
    virtual IInstrument * at (int index) = 0;

    /**
     * Removes all instruments from list.
     */
    virtual void clear (void) = 0;

    /**
     * Returns the currently
     * selected instrument.
     */
    virtual IInstrumentProxy * current (void) = 0;

    /**
     * Promote all channels needed by
     * the current instrument.
     */
    virtual void promoteChannels (void) = 0;

    /**
     * Change the brush scaling factor and
     * update the corresponding widget.
     */
    virtual void chageBrushScale (float d) = 0;
};

/**
 * A factory call.
 */
IInstrumentList * g_fac_SPAWN_INSTRUMENT_LIST (void);
#ifdef QT_VERSION
#   include <QWidget>

IInstrumentList * g_fac_SPAWN_INSTRUMENT_LIST (QWidget * parent = NULL);
#endif

#endif //IINSTRUMENTLIST_H
