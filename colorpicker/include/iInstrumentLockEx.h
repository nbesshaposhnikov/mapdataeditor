/***************************************************************************
 *  Copyright (C) 2009 by Anton R. <commanderkyle@gmail.com>
 *
 *  This file is a part of QAquarelle project.
 *
 *  QAquarelle is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  QAquarelle is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with QAquarelle.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef IINSTRUMENTLOCKEX_H
#define IINSTRUMENTLOCKEX_H

#include "iInstrumentLock.h"
#include "iPaintingStyle.h"
#include "iImageTransaction.h"

class IChannel;


/// Interface IInstrumentLockEx
/// The extended version of an instrument operation descriptor.
class IInstrumentLockEx : public IInstrumentLock {

public:

    /**
     * Current paper texture set to canvas.
     */
    virtual IArray * paperTexture (void) = 0;

    /**
     * Dpi value for the paper texture.
     */
    virtual int paperDpi (void) const = 0;

    /**
     * Returns the pointer to painting
     * style description interface.
     */
    virtual IPaintingStyle * paintingStyle (void) = 0;

    /**
     * Current instrument velocity.
     */
    virtual const float * velocity (void) const = 0;

    /**
     * Interface to current transaction if available.
     */
    virtual ITransaction * transaction (void) = 0;
};

#endif //IINSTRUMENTLOCKEX_H

