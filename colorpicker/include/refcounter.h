/***************************************************************************
 *   Copyright (C) 2008 by Anton R. <commanderkyle@gmail.com>              *
 *                                                                         *
 *   This file is a part of QBlurrk project.                               *
 *                                                                         *
 *   QBlurrk is free software; you can redistribute it and/or modify       *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/
#pragma once

/**
 * Reference counter
 * implementation.
 */
class CRefCounter {
protected:
    LONG refs;

    /**
     * Self-kill.
     */
    virtual void suicide (void) = 0;
public:
    CRefCounter (void) {refs = 1;}

    virtual void addRef (void) {refs++;}
    virtual void release (void) {
        if (--refs < 1)
            suicide ();
    }
};
