#ifndef IGRAVITY_H
#define IGRAVITY_H

/**
 * Gravity control inteface.
 */
class IGravity {
public:

    /**
     * Get the current gravity
     * x-component.
     */
    float x (void) = 0;

    /**
     * Get the current gravity
     * y-component.
     */
    float y (void) = 0;
};

#endif // IGRAVITY_H
