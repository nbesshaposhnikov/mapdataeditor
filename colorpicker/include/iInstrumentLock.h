/***************************************************************************
 *  Copyright (C) 2009 by Anton R. <commanderkyle@gmail.com>
 *
 *  This file is a part of QAquarelle project.
 *
 *  QAquarelle is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  QAquarelle is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with QAquarelle.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef IINSTRUMENTLOCK_H
#define IINSTRUMENTLOCK_H

#include "iArray.h"
#include "iPigmentMixture.h"

#define INSTRUMENT_COLORBUFFER_SIZE      32

enum eDataPlanes {
    DATAPLANE_NONE = 0,
    DATAPLANE_WET  = 1,
    DATAPLANE_DRY  = 2,
    DATAPLANE_FLOW = 4,
    DATAPLANE_BG   = 8
};

/**
 * Parameters for a Perlin
 * noise generator.
 */
struct sNoiseParameters {
    float frequency;
    float amplitude;
    float persistence;
};

/// Interface IInstrumentLock
/// Instrument operation descriptor.
class IInstrumentLock {
public:

    /**
     * Drawing privitive color texture.
     */
    virtual IArray * primitiveColor (void) = 0;

    /**
     * Drawing privitive shape texture.
     */
    virtual IArray * primitiveShape (void) = 0;

    /**
     * Drawing privitive geometry
     * as a vertex buffer.
     */
    virtual GLuint primitiveGeometry (int & nVertices) const = 0;

    /**
     * Rotation angle of the primitive
     * around the Z axis.
     */
    virtual int primitiveRotation (void) const = 0;

    /**
     * Returns the components mask that contain
     * zeros for unused components and the proper
     * multipliers for the rest.
     */
    virtual const float * flowControlComponentMask (void) const = 0;

    /**
     * Instrument bleeding rate.
     */
    virtual float bleedingRate (void) const = 0;

    /**
     * Instrument sucking rate.
     */
    virtual float suckingRate (void) const = 0;

    /**
     * Instrument reservoir size determines the
     * threshold delaying the color feedback.
     */
    virtual float reservoirSize (void) const = 0;

    /**
     * Get the texture sensitivity of
     * the instrument in a range [0, 1].
     */
    virtual float stipplingSensitivity (void) const = 0;

    /**
     * Perlin noise parameters block.
     */
    virtual const sNoiseParameters * noiseParameters (void) const = 0;

    /**
     * Get the scaling factor for
     * the instrument.
     */
    virtual float scalingFactor (void) const = 0;

    /**
     * OR-ed set of flags specifying
     * what data planes should be affected by instrument.
     */
    virtual int affectsDataPlanes (void) const = 0;

    /**
     * Delete this descriptor and unlock the brush.
     * This method must be called once you finished
     * working with descriptor.
     */
    virtual void release (void) = 0;    
};

/**
 * Canvas delegate describes the operations
 * that an instrument can perform on ot.
 */
class ICanvasDelegate {
public:
    /**
     * Change the current selection.
     */
    virtual void setSelection (int x, int y, int w, int h) = 0;

    /**
     * Broadcast a new color to be
     * set as current.
     */
    virtual void pickColor (IPigmentMixture * pig) = 0;

    /**
     * Paint a brush touch on a specific channel.
     */
    virtual void touch (IInstrumentLock * operation) = 0;
};

#endif //IINSTRUMENTLOCK_H

