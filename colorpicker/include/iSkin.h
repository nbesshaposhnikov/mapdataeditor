/***************************************************************************
 *   Copyright (C) 2009 by Anton R. <commanderkyle@gmail.com>              *
 *                                                                         *
 *   This file is a part of QAquarelle project.                            *
 *                                                                         *
 *   QAquarelle is free software; you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/
#ifndef ISKIN_H
#define ISKIN_H

#include "iSpawnedObject.h"
#include "iVerboseObject.h"
#include <QtGui>


/**
 * Application skin inerface.
 * Provides an information of
 * current color scheme and
 * skin images.
 */
class ISkin : public ISpawnedObject {
public:

    /**
     * Set the skin root directory.
     */
    virtual void setRoot (QString path) = 0;

    /**
     * Returns the full path to the skin image
     * with the given name.
     */
    virtual QString pixmapPathByName (QString name) const = 0;

    /**
     * Load an image from the skin data
     * by its name. Images are cached.
     */
    virtual QImage * pixmapByName (QString name) = 0;
};

ISkin * g_fac_SPAWN_SKIN (QString path);

#endif // ISKIN_H
