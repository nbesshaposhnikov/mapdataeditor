/***************************************************************************
 *   Copyright (C) 2009 by Anton R. <commanderkyle@gmail.com>              *
 *                                                                         *
 *   This file is a part of QAquarelle project.                            *
 *                                                                         *
 *   QAquarelle is free software; you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/
#ifndef IPLUGIN_H
#define IPLUGIN_H

#include "iSpawnedObject.h"

/**
 * Possible plugin parameter
 * types.
 */
enum ePLUGIN_PARAM_TYPE {
    PARAM_UNDEFINED,
    PARAM_STRING,
    PARAM_INTEGER,
    PARAM_FLOAT,
    PARAM_VEC2,
    PARAM_VEC3,
    PARAM_VEC4,
    PARAM_IARRAY
};

/**
 * Base plugin interface.
 */
class IPlugin : public ISpawnedObject {

public:
    /**
     * Get the name of this plugin.
     */
    virtual const char * name (void) = 0;

    /**
     * Get the description of this plugin.
     */
    virtual const char * description (void) = 0;

    /**
     * Get the author of this plugin.
     */
    virtual const char * author (void) = 0;

    /**
     * Get the number of custom parameters
     * accepted by the plugin.
     */
    virtual int customParametersCount (void) const = 0;

    /**
     * Get the name of custom parameter
     * with the given index.
     * /return Pointer to string of NULL if the index is out of range.
     */
    virtual const char * customParameterName (int index) const = 0;

    /**
     * Get the type of custom parameter
     * with the given index.
     */
    virtual ePLUGIN_PARAM_TYPE customParameterType (int index) const = 0;

    /**
     * Get the type of custom parameter
     * with the given name.
     */
    virtual ePLUGIN_PARAM_TYPE customParameterType (const char * name) const = 0;

    /**
     * Get the value of custom parameter with the given name.
     * /return TRUE if the value has been successfully retrieved.
     */
    virtual bool customParameterGet (const char * name, void * buffer) = 0;

    /**
     * Set the value of custom parameter with the given name.
     * /return TRUE if the value has been successfully set.
     */
    virtual bool customParameterSet (const char * name, const void * buffer) = 0;

    /**
     * Deinit all OpenGL-related data
     * structures to be prepared for a
     * context switching.
     */
    virtual void deInit (void) = 0;
};

#endif //IPLUGIN_H
