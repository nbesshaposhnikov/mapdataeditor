#ifndef ICONTROLSSTACK_H
#define ICONTROLSSTACK_H


#include "iLogger.h"
#include "iVerboseObject.h"
#include "iSpawnedObject.h"
#include "iArray.h"

#include <QWidget>

///
class IControlsStack : public IVerboseObject,
                       public ISpawnedObject {
public:

    virtual QWidget * asWidget (void) = 0;

    /**
     * Get the widget at
     * given position.
     */
    virtual QWidget * at (int index) = 0;

    /**
     * Push the named widget into the stack.
     */
    virtual void push (QWidget * widget, QString name) = 0;

    /**
     * Switch the widget to be shown,
     */
    virtual void showWidget (QWidget * widget) = 0;

    /**
     * Switch the widget to be hidden,
     */
    virtual void hideWidget (QWidget * widget) = 0;
};

#endif // ICONTROLSSTACK_H
