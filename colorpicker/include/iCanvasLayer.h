/***************************************************************************
 *   Copyright (C) 2009 by Anton R. <commanderkyle@gmail.com>              *
 *                                                                         *
 *   This file is a part of QAquarelle project.                            *
 *                                                                         *
 *   QAquarelle is free software; you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/
#ifndef ICANVASLAYER_H
#define ICANVASLAYER_H

#include "iArray.h"
#include "iChannel.h"
#include "iSpawnedObject.h"
#include "iDataContainer.h"


/**
 * Interface to a canvas layer.
 */
class ICanvasLayer : public ISpawnedObject,
                     public IDataContainer {
public:
    /**
     * Apply an instrument at given position.
     */
    virtual void touch (IInstrumentLockEx * operation, int x, int y) = 0;

    /**
     * Clear all channels in a layer.
     */
    virtual void clear (void) = 0;

    /**
     * Resize the layer.
     */
    virtual void resize (long width, long height) = 0;

    /**
     * Get the layer width.
     */
    virtual long width (void) const = 0;

    /**
     * Get the layer height.
     */
    virtual long height (void) const = 0;

    /**
     * Check if the layer has some
     * background color.
     */
    virtual bool hasBackground (float * rgba) const = 0;

    /**
     * Check if the layer
     * contains the given channel.
     */
    virtual bool hasChannel (FOURCC uid) = 0;

    /**
     * Set the layer dpi.
     */
    virtual void setDpi (int dpi) = 0;

    /**
     * Get the layer dpi.
     */
    virtual int dpi (void) const = 0;

    /********** GET\SET METHODS ***********/

    virtual float opacity (void) const = 0;

    virtual QString name (void) const = 0;

    virtual void setName (QString name) = 0;

    virtual void setOpacity (float opacity) = 0;

    /************** QLIST WRAPPER ***************/

    virtual void append (IChannel * channel) = 0;

    virtual void prepend (IChannel * channel) = 0;

    virtual void removeAt (int index) = 0;

    virtual IChannel * at (int index) = 0;

    virtual int size (void) = 0;
};

/**
 * A factory call.
 */
ICanvasLayer * g_fac_SPAWN_CANVASLAYER (long width, long height, IOpenGLHelper * helper);

#endif // ICANVASLAYER_H
