#ifndef IVERBOSEOBJECT_H
#define IVERBOSEOBJECT_H

#include "iLogger.h"


class IVerboseObject {
public:
    /**
     * Returns the description of last error
     * or NULL if no error occured.
     */
    virtual const char * lastError (void) const = 0;

    /**
     * Attach the logger to an object.
     */
    virtual void attachLogger (ILogger * logger) = 0;

    /**
     * Enable of disable logging.
     */
    virtual void enableLogging (bool enable) = 0;

    /**
     * Check if the logging is enabled.
     */
    virtual bool loggingEnabled (void) = 0;
};

#define _VERBOSEOBJECT \
    bool         m_LoggingEnabled;\
    const char * m_LastError;\
    ILogger    * m_Logger;\
\
    /**\
     * A call to logger object.\
     */\
    void logMessage (eLOG_IMPORTANCE importance, const char * msg, ...) {\
        if (msg && m_Logger && m_LoggingEnabled) {\
            va_list val;\
            va_start (val, msg);\
            m_Logger->write (importance, this, msg, val);\
\
            if (importance == LOGIMPORTANCE_ERROR ||\
                importance == LOGIMPORTANCE_CRITICAL)\
                m_LastError = msg;\
        }\
    }

#endif //IVERBOSEOBJECT_H
