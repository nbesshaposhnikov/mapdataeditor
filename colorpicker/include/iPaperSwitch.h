/***************************************************************************
 *  Copyright (C) 2009 by Anton R. <commanderkyle@gmail.com>
 *
 *  This file is a part of QAquarelle project.
 *
 *  QAquarelle is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  QAquarelle is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with QAquarelle.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef IPAPERSWITCH_H
#define IPAPERSWITCH_H

#include "iLogger.h"
#include "iVerboseObject.h"
#include "iConnector.h"
#include "iArray.h"

#include <QWidget>

#define PREFILTERED_PAPER_COUNT     8


/**
 * Paper description.
 */
struct sPaper {
    enum ePaperClass {
        PLAIN       = 0,
        WATERCOLOR  = 1,
        CANVAS      = 2
    }
    paper_class;        /**< Paper class.       */
    IArray * color;     /**< Paper color map.   */
    IArray * height;    /**< Paper height map.  */
    QString  name;      /**< Paper name.        */
    int      dpi;       /**< Paper texture dpi. */
    int      filtered_count;

    struct sPrefiltered {
        IArray * filtered;      /**< Filtered height map.   */
        float    filter_r;      /**< Filter radius.         */

      // A list of prefiltered paper textures.
    } filtered[PREFILTERED_PAPER_COUNT];
};


/**
 * Interface to a paper switch widget.
 */
class IPaperSwitch : public IVerboseObject,
                     public IConnectorCallback,
                     public IDataContainer {
public:

    virtual QWidget * asWidget (void) = 0;

    /**
     * Get the paper currently
     * selected in widget.
     */
    virtual sPaper * paper (void) = 0;

    /**
     * Find the paper with a given name
     * and set it as current.
     */
    virtual void setByName (QString name) = 0;

    /**
     * Rearrange the papers by their classes
     * and set the first one as current.
     */
    virtual void setByClass (sPaper::ePaperClass c) = 0;
};

#endif // IPAPERSWITCH_H
