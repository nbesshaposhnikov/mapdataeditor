/***************************************************************************
 *   Common include file.                                                  *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/
//#pragma once
#ifndef H_COMMTYPES_H
#define H_COMMTYPES_H

#ifdef WIN32
#   include <windows.h>
//#   ifdef CHAR
//#       undef CHAR
//#   endif
//#   define CHAR        wchar_t
#else
#   define BYTE        unsigned char
#   define CHAR        char
#   define WORD        unsigned short
#   define INT         int
#   define INT32       int  // FIXME
#   define UINT        unsigned int
#   define BOOL        int
#   define LONG        long
#   define ULONG       unsigned long
#   define DWORD       unsigned long
#   define LONGLONG    unsigned long long
#   define FLOAT       float
#   define DOUBLE      double

#   define LPBYTE      unsigned char*
#   define LPVOID      void*

#   define FOURCC      unsigned long
#endif

#endif
