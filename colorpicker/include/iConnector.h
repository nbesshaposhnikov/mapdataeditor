/***************************************************************************
 *   Copyright (C) 2009 by Anton R. <commanderkyle@gmail.com>              *
 *                                                                         *
 *   This file is a part of QAquarelle project.                            *
 *                                                                         *
 *   QAquarelle is free software; you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/
#ifndef ICONNECTOR_H
#define ICONNECTOR_H

#include "iSpawnedObject.h"
#include "iRenderingChain.h"
#include "iConnectorRequests.h"

#define CONNECTOR_PORT          56111


class IConnector;

class IConnectorCallback : public ISpawnedObject {
public:

    /**
     * Pass the connector interface
     * to the object.
     */
    virtual void connect (IConnector * caller) = 0;

    /**
     * A callback, called by connector when
     * the requested data is recieved.
     * @param param Caller-define parameter associated with request.
     * @param length The length of the data string in bytes.
     * @param data Data returned. This buffer will be invalid after the function exit.
     */
    virtual void complitedRequest (int param, int length, void * data) = 0;
};

/**
 * An interface to all server
 * objects.
 */
class IConnector : public ISpawnedObject,
                   public IVerboseObject {
public:
    /**
     * Connect to a server.
     */
    virtual bool connect (const char * host, int port) = 0;

    /**
     * Disconnect from a server.
     */
    virtual bool disconnect (void) = 0;

    /**
     * Get the current connection status.
     */
    virtual bool connected (void) const = 0;

    /**
     * Request an entity allocation.
     */
    virtual void allocateObject (IConnectorCallback * caller, int param,
                                 eENTITY_TYPE objtype, const char * classname = NULL) = 0;

    /**
     * Parse the pigments descriprions and return
     * the pack via complitedRequest ().
     */
    virtual void parsePigmentsPack (IConnectorCallback * caller, int param, const BYTE * data, int length) = 0;

    /**
     * If connector is set into the blobcking
     * state it won't process any messages from
     * server, to ensure that waitObject () gets
     * them.
     */
    virtual void setBlobcking (bool blocking) = 0;

    /**
     * Send a request to a remote object.
     * @param remoteId Id of the object on server side.
     * @param request Request code.
     * @param length Length of the input data in bytes.
     * @param data Input data buffer.
     * @return TRUE if the call was successful.
     */
    virtual bool callObject (int remoteId, int request, int length, const BYTE * data) = 0;

    /**
     * Wait for the response of a remote object.
     * @param remoteId Id of the object on server side.
     * @param data Output data buffer.
     * @param interval Number of milliseconds to wait.
     * @return The number of bytes recieved or 0 if error occured.
     */
    virtual int waitObject (int remoteId, BYTE * data, int interval = 1024) = 0;

    /**
     * Notify all objects that we're going
     * to lose the context.
     */
    virtual void detachfromContext (void) = 0;

    /**
     * Request all objects the reload their
     * shaders now.
     */
    virtual void reloadShaders (void) = 0;
};

/**
 * A factory call.
 */
IConnector * g_fac_SPAWN_CONNECTOR (IOpenGLHelper * helper);

#endif // ICONNECTOR_H
