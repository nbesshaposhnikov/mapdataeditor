/***************************************************************************
 *  Copyright (C) 2009 by Anton R. <commanderkyle@gmail.com>
 *
 *  This file is a part of QAquarelle project.
 *
 *  QAquarelle is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  QAquarelle is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with QAquarelle.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef IRENDERINGCHAIN_H
#define IRENDERINGCHAIN_H

#include "iChannel.h"
#include "iSpawnedObject.h"
#include "iVerboseObject.h"
#include "iOpenGLHelper.h"

enum eRenderingOptions {
    RENDER_PAINTS           = 1,
    RENDER_PAPER_BUMP       = 2,
    RENDER_PAINT_BUMP       = 4,
    RENDER_MOTION_VECTORS   = 8,
    RENDER_EVERYTHING       = 15
};

/**
 * A descriptor for a
 * rendering operation.
 */
struct sRenderOperation {
    int roi_x;
    int roi_y;
    int roi_w;
    int roi_h;

    int target_w;
    int target_h;
    int target_dpi;

    int plane_width;
    int plane_height;

    float view_translate[3];    /**< Canvas plane translation in a view space.  */
    float view_rotate[3];       /**< Canvas plane rotation in a view space.     */
    float view_scale[3];        /**< Canvas plane scaling in a view space.      */
};


/**
 * A layer representation in
 * rendering chain.
 */
class IRenderingChainLayer {
public:
    /**
     * Set the layer opacity.
     */
    virtual void setOpacity (float opacity) = 0;

    /**
     * Set the layer dpi.
     */
    virtual void setLayerDpi (int dpi) = 0;

    /**
     * Set the backgroud color for this layer.
     * @param bg Pointer to an RGBA data or NULL to unset color.
     */
    virtual void setBackgroundColor (const float * bg) = 0;

    /**
     * Get the channel at given position.
     */
    virtual IChannel * at (int index) = 0;

    /**
     * X-offset of the layer.
     */
    virtual float offsetX (void) const = 0;

    /**
     * Y-offset of the layer.
     */
    virtual float offsetY (void) const = 0;

    /**
     * Layer width.
     */
    virtual float width (void) const = 0;

    /**
     * Layer height.
     */
    virtual float height (void) const = 0;

    /**
     * Check if the layer has filled
     * with background color before rendeing.
     */
    virtual const float * hasBackgroundColor (void) const = 0;

    /**
     * Insert the channel at the
     * begining of the list.
     */
    virtual void append (IChannel * channel) = 0;

    /**
     * Insert the channel at the
     * end of the list.
     */
    virtual void prepend (IChannel * channel) = 0;

    /**
     * Remove the channel
     * at given position.
     */
    virtual void removeAt (int index) = 0;

    /**
     * Get the number of channels
     * in this layer.
     */
    virtual int size (void) const = 0;

    /**
     * Remove all channels from the layer.
     */
    virtual void clear (void) = 0;

    /**
     * Get the layer dpi;
     */
    virtual int dpi (void) const = 0;
};

/// RenderingChain is used to build up a pixmap from the
/// stack of data channels.
class IRenderingChain : public ISpawnedObject,
                        public IVerboseObject {

public:
    /**
     * Renders all channels onto a raster
     * of requested format and returns the
     * raster handle as IArray interface.
     */
    virtual bool render (UINT glTarget, const sRenderOperation * operation) = 0;

    /**
     * Create a new layer.
     * Returns an interface to a new layer that
     * you can use to pack the channels into.
     * @param x Normalized x-offset of the layer.
     * @param y Normalized y-offset of the layer.
     * @param x Normalized width of the layer.
     * @param y Normalized height of the layer.
     */
    virtual IRenderingChainLayer * addLayer (float x, float y, float w, float h) = 0;

    /**
     * Set the bump map that will be
     * used in final rendering iteration.
     */
    virtual void setBumpMap (IArray * map, int paperdpi) = 0;

    /**
     * Get the current filtering method
     * used in a rendering process.
     */
#ifdef QT_OPENGL_LIB
    virtual GLenum filteringMethod (void) const = 0;
#else
    virtual unsigned int filteringMethod (void) const = 0;
#endif

    /**
     * Set the fltering method that
     * will be used in a rendering process.
     */
#ifdef QT_OPENGL_LIB
    virtual void setFilteringMethod (GLenum filtering) = 0;
#else
    virtual void setFilteringMethod (unsigned int filtering) = 0;
#endif

    /**
     * Remove all layers from the chain.
     */
    virtual bool clear (void) = 0;

    /**
     * Set the light position.
     * Used for bump mapping implementation.
     */
    virtual void setLightPosition (float x, float y, float z) = 0;

    /**
     * Reload all OpenGL shaders.
     */
    virtual void reloadShaders (void) = 0;

    /**
     * Set the rendered thickness of paint.
     * This controls the amount of bump mapping
     * applied to a wet chanel.
     */
    virtual void setPaintThickness (float z) = 0;

    /**
     * Enable the given rendering options.
     */
    virtual void enableOptions (int flags) = 0;

    /**
     * Disable the given rendering options.
     */
    virtual void disableOptions (int flags) = 0;

    /**
     * Get the current rendering options.
     */
    virtual int renderingOptions (void) const = 0;
};

/// A factory call.
IRenderingChain * g_fac_SPAWN_RENDERING_CHAIN (IOpenGLHelper * helper);

#endif //IRENDERINGCHAIN_H

