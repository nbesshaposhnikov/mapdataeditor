#ifndef ILOGGER_H
#define ILOGGER_H

#include "iSpawnedObject.h"
#include <stdarg.h>

// Namespace

enum eLOG_IMPORTANCE {
    LOGIMPORTANCE_NEVERMIND,
    LOGIMPORTANCE_WARNING,
    LOGIMPORTANCE_ERROR,
    LOGIMPORTANCE_CRITICAL
};

/// Interface ILogger.
class ILogger : public ISpawnedObject {
// Public stuff
public:

    /**
     * Write the log entry for a specific object.
     */
    virtual void write (
            eLOG_IMPORTANCE importance,
            ISpawnedObject * caller,
            const char * str, ...
    ) = 0;

    /**
     * Write the log entry for a specific object.
     */
    virtual void write (
            eLOG_IMPORTANCE importance,
            ISpawnedObject * caller,
            const char * message,
            va_list val
    ) = 0;

};


#endif //ILOGGER_H
