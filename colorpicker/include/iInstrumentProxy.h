#ifndef IINSTRUMENTPROXY_H
#define IINSTRUMENTPROXY_H

#include "iInstrument.h"
#include "qhasmath.h"

/// A proxy interface to
/// an instrument plugin.
class IInstrumentProxy : public IInstrument {
public:

    /**
     * Get the scaling factor modifier
     * associated with the instrument.
     */
    virtual float scaleModifier (void) const = 0;

    /**
     * Get the texture sensitivity modifier
     * associated with the instrument.
     */
    virtual float textureSensitivityModifier (void) const = 0;

    /**
     * Set the scaling factor modifier
     * associated with the instrument.
     */
    virtual void setScaleModifier (float scale) = 0;

    /**
     * Set the texture sensitivity modifier
     * associated with the instrument.
     */
    virtual void setTextureSensitivityModifier (float sensitivity) = 0;

    /**
     * Get the X-component of
     * current brush velocity.
     */
    virtual float velocityX (void) const = 0;

    /**
     * Get the Y-component of
     * current brush velocity.
     */
    virtual float velocityY (void) const = 0;

    /**
     * Get the point of last stoke.
     * @param alpha Weight [0, 1].
     * @param x Out: x-position between last two points.
     * @param y Out: y-position between last two points.
     */
    virtual void stroke (float alpha, int & x, int & y) = 0;

    /**
     * Reset all accumulated information.
     * THis method is usually called on the
     * end of a stroke.
     */
    virtual void reset (void) = 0;

    /**
     * Set the amount of jitter to
     * apply to an instrument.
     */
    virtual void applyJitter (float amount) = 0;

    /**
     * Notify a proxy when the instrument
     * is attached to current pipeline.
     */
    virtual void attached (void) = 0;

    /**
     * Notify a proxy when the instrument
     * is detached from current pipeline.
     */
    virtual void detached (void) = 0;

    /**
     * Get the current jitter value.
     */
    virtual float jitter (void) const = 0;

    /**
     * Get current pressure value.
     */
    virtual float pressure (void) = 0;

    /**
     * Returns QImage * casted to (void *).
     */
    virtual const void * qimage (void) = 0;

    /**
     * Reset the brush traveling meter.
     */
    virtual void meterReset (void) = 0;

    /**
     * Reset the brush anchored meter.
     */
    virtual void anchoredMeterReset (void) = 0;

    /**
     * Brush traveled distance in pixels.
     */
    virtual long meter (void) const = 0;

    /**
     * Brush distance from the last anchor in pixels.
     */
    virtual long anchoredMeter (void) const = 0;

    /**
     * Get the current pressure mapping curve.
     */
    virtual ISpline * pressureCurve (void) = 0;

    /**
     * Get the underlying instrument object.
     */
    virtual IInstrument * soul (void) = 0;
};

/**
 * A factory call.
 */
IInstrumentProxy * g_fac_SPAWN_INSTRUMENT_PROXY (IInstrument * instrument);

#endif //IINSTRUMENTPROXY_H

