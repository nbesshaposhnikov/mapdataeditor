#ifndef QHASGLOBAL_H
#define QHASGLOBAL_H

namespace QHASGlobal {
    enum eStatus {
        STATUS_IDLE,                /**< Nothing happens.                       */
        STATUS_PRIORITY_USER,       /**< User is performing some activity.      */
        STATUS_PRIORITY_SYSTEM,     /**< System is performing some activity.    */
        STATUS_DEAD                 /**< Some error ocuured.                    */
    };
}

#endif // QHASGLOBAL_H
