/***************************************************************************
 *   Copyright (C) 2009 by Anton R. <commanderkyle@gmail.com>              *
 *                                                                         *
 *   This file is a part of QAquarelle project.                            *
 *                                                                         *
 *   QAquarelle is free software; you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/
#ifndef ILAYERSLIST_H
#define ILAYERSLIST_H

#include "iVerboseObject.h"
#include "iSpawnedObject.h"
#include "iConnector.h"

#include "iCanvas.h"
#include "iCanvasLayer.h"

class ILayersList : public IVerboseObject,
                    public IConnectorCallback {
public:
    virtual QWidget * asWidget (void) = 0;

    /**
     * Attach the canvas object that will
     * mirror all changes in the list,
     */
    virtual void attach (ICanvas * canvas) = 0;

    /**
     * Detach the list from canvas object
     * if attached.
     */
    virtual void detach (void) = 0;

    /**
     * Add a new layer to
     * the end of the list.
     */
    virtual void append (ICanvasLayer * layer) = 0;

    /**
     * Add a new layer to
     * the beginning of the list.
     */
    virtual void prepend (ICanvasLayer * layer) = 0;

    /**
     * Remove a layer at
     * given position.
     */
    virtual void removeAt (int index) = 0;

    /**
     * Remove the specified layer
     * from list.
     */
    virtual void remove (ICanvasLayer * target) = 0;

    /**
     * Returns a layer  at given position
     * or NULL if position is illegal ar empty.
     */
    virtual ICanvasLayer * at (int index) = 0;

    /**
     * Removes all layers from list.
     */
    virtual void clear (void) = 0;

    /**
     * Returns the currently
     * selected layer.
     */
    virtual ICanvasLayer * current (void) = 0;
};

/**
 * A factory call.
 */
ILayersList * g_fac_SPAWN_LAYER_LIST (void);

#   ifdef QT_VERSION
#       include <QWidget>

ILayersList * g_fac_SPAWN_LAYER_LIST (QWidget * parent = NULL);

#   endif // QT_VERSION
#endif // ILAYERSLIST_H
