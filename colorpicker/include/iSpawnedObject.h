#ifndef ISPAWNEDOBJECT_H
#define ISPAWNEDOBJECT_H

//#include <iostream>


class ISpawnedObject {
public:
    /**
     * Get the classname for this object.
     */
    virtual const char * myClassName (void) = 0;

    /**
     * Get the Id of current allocated object.
     */
    virtual int objectId (void) = 0;

    /**
     * Increase the object's
     * reference counter.
     */
    virtual void addRef (void) = 0;

    /**
     * Release the object.
     */
    virtual void release (void) = 0;

    /**
     * Ask the object to emit all
     * notification signals if any.
     */
    virtual void pokeObject (void) = 0;

    /**
     * Next object in allocation list.
     */
    ISpawnedObject * next (void) {
        return m_Next;
    }

    void linkTo (ISpawnedObject * obj) {
        m_Next = obj;
    }

    static ISpawnedObject * orphans (void);

    static int totalObjects (void);
    static int allocateId   (void);
/*
    void * operator new (size_t sz, const char * file=__FILE__, int line=__LINE__)
           throw (std::bad_alloc);
    void operator delete (void * p) throw ();
*/
protected:
    ISpawnedObject * m_Next;

    static void _register   (ISpawnedObject * me);
    static void _unregister (ISpawnedObject * me);
};

#endif // ISPAWNEDOBJECT_H
