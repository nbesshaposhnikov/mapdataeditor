/***
 *  Copyright (C) 2009 Anton R. <commanderkyle@gmail.com>
 *
 *  This file is a part of QAquarelle project.
 *
 *  QAquarelle is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  QAquarelle is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with QAquarelle.  If not, see <http://www.gnu.org/licenses/>.
 */
#pragma once

//#ifdef QT_OPENGL_LIB
#   define UINT_UNDEFINED        ((UINT)-1)
#   define BUFFER_OFFSET(i)      ((char *)NULL + (i))

#   include "iVerboseObject.h"
#   include "iSpawnedObject.h"
#   ifdef QT_OPENGL_LIB
#       include <QHash>
#       include <QMutex>
#   endif

enum eGPU_VENDOR {
    GPUVENDOR_UNKNOWN,
    GPUVENDOR_NVIDIA,
    GPUVENDOR_ATI
};

/**
 * Shared precompiled lists enumerator.
 */
enum eSharedList {
    OPENGLHELPER_LIST_QUAD1      = 0,
    OPENGLHELPER_LIST_QUAD1_X0Y0 = 1,
    OPENGLHELPER_LIST_QUAD1_X1Y0 = 2,
    OPENGLHELPER_LIST_QUAD1_X0Y1 = 3,
    OPENGLHELPER_LIST_QUAD1_X1Y1 = 4
};

/**
 * Shared FBOs enumerator.
 */
enum eSharedFBO {
    OPENGLHELPER_FBO_PATCH   = 0,        /**< An FBO for on-patch rendering.            */
    OPENGLHELPER_FBO_GENERIC = 1         /**< An FBO for a generic offscreen rendering. */
};

/**
 * Shared textures enumerator.
 */
enum eSharedTexture {
    OPENGLHELPER_TEX_NOISE0 = 0,        /**< Base texture for Perlin noise.     */
    OPENGLHELPER_TEX_NOISE1 = 1         /**< Base texture for Perlin noise.     */
};

enum eOGL_PERFFLAG {
    OPENGLHELPER_TEX_COMPRESSION = 0,   /**< Texture compression. */
    OPENGLHELPER_PERFFLAGS_COUNT
};

enum eTextureType {
    OPENGLHELPER_TEXTURE_RGBA,
    OPENGLHELPER_TEXTURE_RGB,
    OPENGLHELPER_TEXTURE_RGBA_FLOAT,
    OPENGLHELPER_TEXTURE_LUMINANCE,
    OPENGLHELPER_TEXTURE_LUMINANCE_FLOAT
};

#ifndef QT_OPENGL_LIB
#   define GLuint               unsigned int
#   define GLint                int
#   define GLenum               int
#   define GL_WRITE_ONLY_ARB    0x88B9
#   define GL_READ_WRITE_ARB    0x88BA
#   define GL_NEAREST           0x2600
#endif

#ifdef GLEW_MX
#   define _GLEW_ENABLED \
    GLEWContext * glewGetContext (void) {\
        return m_Helper->context ();\
    }
#else
#   define _GLEW_ENABLED
#endif


class IOpenGLShader : public ISpawnedObject {
public:

    /**
     * Get the OpenGL identifier
     * of program.
     */
#ifdef QT_OPENGL_LIB
    virtual GLuint handle (void) const = 0;
#else
    virtual unsigned long handle (void) const = 0;
#endif

    virtual int getUniformLocation (const char * name) = 0;
    virtual void uniform1i         (int location, int v0) = 0;
    virtual void uniform1f         (int location, float v0) = 0;
    virtual void uniform2f         (int location, float v0, float v1) = 0;
    virtual void uniform3f         (int location, float v0, float v1, float v2) = 0;
    virtual void uniform4f         (int location, float v0, float v1, float v2, float v3) = 0;
    virtual void uniform4fv        (int location, int sz, const float * v) = 0;
    virtual void useProgram        (void) = 0;
    virtual void unuseProgram      (void) = 0;

    /**
     * Read the shader source once again
     * and recompile it.
     */
    virtual bool reload (void) = 0;
};


class IArray;

class IFBOWrapper :  public ISpawnedObject,
                     public IVerboseObject {
public:
    /**
     * Bind the framebuffer object.
     */
    virtual void bind (void) = 0;

    /**
     * Get the FBO name.
     */
    virtual GLuint handle (void) const = 0;

    /**
     * Mark all attachments as free.
     */
    virtual void reset (void) = 0;

    /**
     * Attach the texture and return an
     * attachment point used.
     */
    virtual GLenum attachTexture (GLuint tex, int priority = 0) = 0;

    /**
     * Attach the texture to the given point.
     */
    virtual bool attachTexture_explicit (GLuint tex, int point) = 0;

    /**
     * Detach the texture.
     */
    virtual void detachTexture (GLuint tex) = 0;
};


class IOpenGLHelper : public IVerboseObject,
                      public ISpawnedObject {
public:
    
    /**
     * Init the OpenGL stuff.
     */
    virtual void init (void) = 0;

    /**
     * Deinit.
     */
    virtual void deinit (void) = 0;

    /**
     * Check the OpenGL status.
     * Returns TRUE if successfully inited.
     */
    virtual bool inited (void) const = 0;

    /**
     * Set some rendering options.
     */
    virtual void applyRenderingParams (void) = 0;

    /**
     * Set the shaders source directory.
     */
    virtual void setShadersPath (const char * path) = 0;

    /**
     * Load the shader. A program with
     * a default vertex shader and the given
     * fragment shader is generated,
     */
    virtual IOpenGLShader * loadShader (const char * path) = 0;

    /**
     * Load the shader. A program with
     * the given vertex and fragment
     * shaders is generated,
     */
    virtual IOpenGLShader * loadShader (const char * vert, const char * frag) = 0;

    /*
     * Check for the GL extension
     */
    virtual bool checkExtension (const char * extension) = 0;

    /**
     * Get the current OpenGL context.
     */
#ifdef GLEW_MX
#ifdef QT_OPENGL_LIB
    virtual GLEWContext * context (void) = 0;
#else
    virtual void * context (void) = 0;
#endif
#endif
    virtual void bindTexture_ex (GLuint tex, int idx,
                                     GLuint filter = GL_NEAREST,
                                     bool   border = FALSE) = 0;

    virtual const char * checkFramebufferStatus (void) = 0;
    virtual const char * checkGLErrors (void) = 0;

    /**
     * Get the vendor of a host video card.
     */
    virtual eGPU_VENDOR gpuVendor (void) const = 0;

    /**
     * Returns one of the pregenerated
     * shared lists by its id.
     */
    virtual GLuint sharedList (eSharedList id) const = 0;

    /**
     * Returns one of the pregenerated
     * shared framebuffers.
     */
    virtual IFBOWrapper * sharedFBO (eSharedFBO id) = 0;

    /**
     * Returns one of the pregenerated
     * shared textures.
     */
    virtual IArray * sharedTexture (eSharedTexture id) = 0;

    /**
     * Get the size of the shared patches
     * in pixels. All processing around here
     * shuold use this patch size.
     */
    virtual int sharedPatchSize (void) const = 0;

    /**
     * Allocates a temporary texture of a
     * predefined patch-size, locks it and
     * returns to caller.
     */
    virtual GLuint sharedPatchLock (void) = 0;

    /**
     * Swap the texture name of the given
     * patch with the new name.
     */
    virtual GLuint sharedPatchSwap (GLuint old_name, GLuint new_name) = 0;

    /**
     * Mark the texture as free.
     * must be called when the patch is not needed
     * anymore.
     */
    virtual void sharedPatchUnlock (GLuint name) = 0;

    /**
     * A cache for a tempopary storage of
     * the patches that are often accessed.
     */
#ifdef QT_OPENGL_LIB
    virtual QHash<unsigned long, GLuint> * sharedPatchCache (void) = 0;
#else
    virtual void * sharedPatchCache (void) = 0;
#endif

    /**
     * Shared mutex to sync all calls.
     */
#ifdef QT_OPENGL_LIB
    virtual QMutex * mutex (void) = 0;
#else
    virtual void * mutex (void) = 0;
#endif

    /**
     * Release all loaded shaders.
     */
    virtual void unloadShaders (void) = 0;
    
    /**
     * Propose the internal texture format
     * to represent the given color type.
     */
    virtual GLenum proposeTextureFormat (eTextureType type) = 0;

    /**
     * Get current state of a performance parameter.
     */
    virtual int performanceParameter (eOGL_PERFFLAG flag) const = 0;

    /**
     * Set the new state for a performance parameter.
     */
    virtual void setPerformanceParameter (eOGL_PERFFLAG flag, int value) = 0;

    /**
     * Notify the helper of texture creation event.
     */
    virtual void textureCreationCallback (GLenum format_rq,
                                          GLenum format_real,
                                          bool   compressed) = 0;

    /**
     * Returns the pregenerated
     * PBO for faster patch transfers.
     */
  //virtual GLuint sharedPBO (void) const = 0;
};

#ifdef QT_OPENGL_LIB

/**
 * A factory call.
 */
IOpenGLHelper * g_fac_SPAWN_OPENGL_HELPER (void);

#endif
