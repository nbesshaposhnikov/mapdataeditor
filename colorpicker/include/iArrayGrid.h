/***************************************************************************
 *  Copyright (C) 2009 by Anton R. <commanderkyle@gmail.com>
 *
 *  This file is a part of QAquarelle project.
 *
 *  QAquarelle is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  QAquarelle is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with QAquarelle.  If not, see <http://www.gnu.org/licenses/>.
 */
#pragma once
#include "qhasmath.h"
#include "iArray.h"


enum ePatchStatus {
    PATCHSTAT_NONE    = 0,
    PATCHSTAT_UNKNOWN = 1,
    PATCHSTAT_FILLED  = 2,
    PATCHSTAT_WET     = 4
};

/**
 * Provides a proxy interface to
 * an IArray data patch.
 */
class IArrayGridPatch {
public:

    /**
     * X-offset of the patch in grid.
     */
    virtual int cellX (void) const = 0;

    /**
     * Y-offset of the patch in grid.
     */
    virtual int cellY (void) const = 0;

    /**
     * Rectangle in pixels occupied by the patch.
     */
    virtual const sRect * rect (void) = 0;

    /**
     * Get the patch as IArray interface.
     */
    virtual IArray * data (void) = 0;

    /**
     * Get the current status of the patch.
     */
    virtual int status (void) const = 0;

    /**
     * Update the status infromation
     * according to current data.
     */
    virtual void checkStatus (void) = 0;

    /**
     * Mark the patch as touched by
     * instrument. This resets its status.
     */
    virtual void touched (void) = 0;
};

/**
 * 2D grid of the IArray patches.
 * Used to avoid the huge textures that
 * cause only truobles. =)
 */
class IArrayGrid : public ISpawnedObject {
public:
    virtual int nPatchesX (void) const = 0;
    virtual int nPatchesY (void) const = 0;

    /**
     * Get the patch as IArrayGridPatch interface.
     */
    virtual IArrayGridPatch * patch (int cellx, int celly) = 0;

    /**
     * Get the list of patches that are
     * touched by the given rectanngle.
     * Caller doesn't have to release the returned patches.
     * @return The number of patches written to dest.
     */
    virtual int touched (const sRect * rect, IArrayGridPatch ** dest) = 0;

    /**
     * Get the patch that is touched by
     * the given point.
     * @returns Patch pointer of NULL if out of range.
     */
    virtual IArrayGridPatch * touched (int x, int y) = 0;

    /**
     * Convinience function.
     */
    virtual int touched (int x, int y, int w, int h, IArrayGridPatch ** dest) = 0;

    /**
     * The width of entire data plane.
     */
    virtual long width (void) const = 0;

    /**
     * The heigth of entire data plane.
     */
    virtual long height (void) const = 0;

    /**
     * Size of a single array element.
     */
    virtual long element_size (void) const = 0;

    /**
     * Fill all patches with zeros.
     */
    virtual void zero (void) = 0;

    /**
     * Fill all patches with given data.
     */
    virtual void fill (BYTE r, BYTE g, BYTE b, BYTE a) = 0;

    /**
     * Fill all patches with given data.
     */
    virtual void fill (float r, float g, float b, float a) = 0;
};

#ifdef QT_OPENGL_LIB
IArrayGrid * g_fac_SPAWN_ARRAYGRID (IOpenGLHelper * helper, LONG width, LONG height,
                                    GLenum format, GLenum type = GL_UNSIGNED_BYTE);//GL_FLOAT
#endif
