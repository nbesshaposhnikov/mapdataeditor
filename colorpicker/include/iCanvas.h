/***************************************************************************
 *   Copyright (C) 2009 by Anton R. <commanderkyle@gmail.com>              *
 *                                                                         *
 *   This file is a part of QAquarelle project.                            *
 *                                                                         *
 *   QAquarelle is free software; you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/
#ifndef ICANVAS_H
#define ICANVAS_H

#include "iLogger.h"
#include "iSpawnedObject.h"
#include "iVerboseObject.h"
#include "iArray.h"
#include "iFilter.h"
#include "iPipeline.h"
#include "iUndostackProxy.h"
#include "iInstrumentLock.h"
#include "iInstrumentProxy.h"
#include "iCanvasLayer.h"
#include "iConnector.h"
#include "iDataContainer.h"
#include "iScrollarea.h"
#include "iPaperSwitch.h"
#include "qhasglobal.h"
#include "iKeys.h"



/// The main client-side class.
/// It encapsulates all HCI operations on a drawing area.
class ICanvas : public IVerboseObject,
                public IConnectorCallback,
                public IScrollarea,
                public IDataContainer {

public:

    virtual QWidget * asWidget (void) = 0;

    /**
     * Get the real width of a
     * canvas plane.
     */
    virtual long realWidth (void) const = 0;

    /**
     * Get the real height of a
     * canvas plane.
     */
    virtual long realHeight (void) const = 0;

    /**
     * Change the canvas size.
     * If the canvas is not empty, it will try to
     * resize all the underlying data structures.
     */
    virtual void resize (long width, long height) = 0;

    /**
     * Erase all canvas contents.
     */
    virtual void erase (void) = 0;

    /**
     * Set the paper texture to canvas.
     */
    virtual void setPaperTexture (sPaper * texture) = 0;

    /**
     * Get current paper texture.
     */
    virtual const sPaper * paperTexture (void) const = 0;

    /**
     * Apply the custom filter on canvas data.
     */
    virtual void applyFilter (IFilter * filter) = 0;

    /**
     * Applies the given pipeline to all
     * data layers.
     * /return TRUE if the canvas has been changed.
     */
    virtual bool applyPipeline (IPipeline * pipe, float dt) = 0;

    /**
     * Returns the interface to Undo Stack object
     * associated with the canvas.
     */
    virtual IUndostackProxy * getUndoStack (void) = 0;

    /**
     * Set the gravity vector that
     * that will affect the water simulation.
     */
    virtual void setGravity (float x, float y) = 0;

    /**
     * Override the current painting style.
     * Pointer to interface must be always
     * available to the canvas.
     */
    virtual void setPaintingStyle (IPaintingStyle * style) = 0;

    /**
     * Get the current layer.
     * /return The pointer to layer interface or NULL.
     */
    virtual ICanvasLayer * currentLayer (void) = 0;

    /**
     * Create a new layer with the dimentions of
     * canvas and return it without adding to the list.
     */
    virtual ICanvasLayer * allocateNewLayer (void) = 0;

    /**
     * Request the addition of a given channel
     * to the current layer if it is not already
     * present there.
     */
    virtual bool promoteChannel (FOURCC uid) = 0;

    /**
     * Show or hide the color mixer.
     */
    virtual void setMixerVisibility (bool visible) = 0;

    /**
     * Check if the stroke shaping is enabled.
     * This mode will try to dynamically scale the
     * brush if it is controlled my mouse to make the
     * stroke look more realistic.
     */
    virtual bool isSmartStrokeEnabled (void) const = 0;

    /**
     * Enable or disable the brush stroke shaping.
     */
    virtual void setSmartStrokeEnabled (bool enable) = 0;

    /**
     * Deallocate all objects and prepare the
     * canvas for termination.
     */
    virtual void deInit (void) = 0;

    /**
     * Set the mouse dragging action.
     */
    virtual void setDragMode (eDragMode mode) = 0;

    /**
     * Set the action for a mouse button.
     */
    virtual void setMouseAction (int button, eDragMode action) = 0;

    /**
     * Get the action for a mouse button.
     */
    virtual eDragMode getMouseAction (int button) = 0;

    /**
     * Set the image dpi.
     * This may also automatically change the dpi
     * for all layers.
     */
    virtual void setDpi (int dpi, bool layers = FALSE) = 0;

    /**
     * Get the image dpi.
     */
    virtual int dpi (void) const = 0;

    /*********** QLIST WRAPPER **************/

    virtual void append   (ICanvasLayer * layer) = 0;
    virtual void prepend  (ICanvasLayer * layer) = 0;
    virtual void removeAt (int index) = 0;
    virtual void clear    (void) = 0;

    virtual int layersCount (void) = 0;

    virtual ICanvasLayer * at (int index) = 0;

    /*****************************************/

    /**
     * Export the canvas contents as an image.
     * Returns an image that must be released by caller,
     * or NULL if any error ocurred.
     */
    virtual IArray * asImage (void) = 0;

    /**
     * Export the canvas contents to a
     * specified target.
     */
    virtual IArray * asImage (IArray * target) = 0;

    /**
     * Get the current pressure working range.
     * @returns Pointer to min and max values.
     */
    virtual const float * pressureWorkingRange (void) const = 0;

    /**
     * Set the pressure working range.
     */
    virtual void setPressureWorkingRange (float minp, float maxp) = 0;

    /**
     * Move all paint onto the dry layer.
     */
    virtual bool dry (void) = 0;

    /**
     * Enumerator for all patches of the
     * current layer.
     * @returns Patch pointer or NULL if index is out of range.
     */
    virtual IArrayGridPatch * patchIterator (int index) = 0;

    /**
     * Change the current instrument.
     */
    virtual void setInstrument (IInstrumentProxy * instrument) = 0;

    /**
     * Enable the rendering options for current
     * rendering chain.
     */
    virtual void enableRenderingOptions (int flags) = 0;
    
    /**
     * Disable the rendering options for current
     * rendering chain.
     */
    virtual void disableRenderingOptions (int flags) = 0;
};

ICanvas * g_fac_SPAWN_CANVAS (QWidget * parent, int width, int height, IOpenGLHelper * helper);
ICanvas * g_fac_SPAWN_CANVAS (int width, int height, IOpenGLHelper * helper);

#endif //ICANVAS_H
