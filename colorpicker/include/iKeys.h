/***************************************************************************
 *  Copyright (C) 2009 by Anton R. <commanderkyle@gmail.com>
 *                 and by Vasiliy M. <drmoriarty@gmail.com>
 *
 *  This file is a part of QAquarelle project.
 *
 *  QAquarelle is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  QAquarelle is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with QAquarelle.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef IKEY_H
#define IKEY_H

/**
 * Possible drag actions.
 */
enum eDragMode {
    DRAGSTATE_NOOP          = 0,
    DRAGSTATE_CANVAS_MOVE   = 1,
    DRAGSTATE_CANVAS_SCALE  = 2,
    DRAGSTATE_CANVAS_ROTATE = 3,
    DRAGSTATE_BRUSH_SCALE   = 4,
    DRAGSTATE_BRUSH_ROTATE  = 5,
    DRAGSTATE_PAINT         = 6,
    DRAGSTATE_COUNT
};

// And actions for mouse wheel
typedef enum {
	WheelDoesNothing = 0,
	WheelScalesBrush,
	WheelScalesCanvas,
	WheelCOUNT
} WheelAction;

#endif //IKEY_H
