/***************************************************************************
 *   Copyright (C) 2009 by Anton R. <commanderkyle@gmail.com>              *
 *                                                                         *
 *   This file is a part of QAquarelle project.                            *
 *                                                                         *
 *   QAquarelle is free software; you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/
#ifndef IPIGMENTDESCRIPTION_H
#define IPIGMENTDESCRIPTION_H

#include "commdefs.h"
#include "commtypes.h"
#include "refcounter.h"

#include "iPigmentMixture.h"
#include "iPaintingStyle.h"


class IPigmentDescription : public IPigmentMixture {
public:

    /**
     * Generate the extended color description
     * based on the given painting style.
     */
    virtual void implicitFromColor (IPaintingStyle * style, const float * rgb) = 0;

    /**
     * Generate the extended color description
     * based on the given painting style.
     */
    virtual void implicitFromColor (IPaintingStyle * style,
                                    float r, float g, float b) = 0;

    /**
     * Convert the extended color description
     * to RGB data using the given painting style.
     */
    virtual void implicitToColor (IPaintingStyle * style, float * rgb) = 0;

    virtual void setAbsorbing     (const TCC * a) = 0;
    virtual void setScattering    (const TCC * s) = 0;
    virtual void setName          (const char * name) = 0;
    virtual void setWeight        (float v) = 0;
    virtual void setGranulation   (float v) = 0;
    virtual void setStainingPower (float v) = 0;
    virtual void setLightfastness (float v) = 0;

    static float opacity       (ePAINTINGSTYLE_PRESET preset);
    static float viscosity     (ePAINTINGSTYLE_PRESET preset);
    static float dryingspeed   (ePAINTINGSTYLE_PRESET preset);
    static float stainingPower (ePAINTINGSTYLE_PRESET preset);
};

IPigmentDescription * g_fac_SPAWN_PIGMENT_DESCRIPTION (void);

#endif // IPIGMENTDESCRIPTION_H
