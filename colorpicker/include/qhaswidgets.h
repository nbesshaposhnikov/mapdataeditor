#ifndef QHASWIDGETS_H
#define QHASWIDGETS_H

#include "commdefs.h"
#include "commtypes.h"
#include "refcounter.h"

//#ifdef QT_OPENGL_LIB
//#   include "../../../glew/include/GL/glew.h"

#include "iPaperSwitch.h"
#include "iControlsStack.h"


class QHASWidgets {
public:

    /**
     * Init the library's resources.
     */
    static void init (void);

    static IPaperSwitch   * spawnPaperSwitch   (QWidget * parent);
    static IPaperSwitch   * spawnPaperSwitch   (void);
    static IControlsStack * spawnControlsStack (QWidget * parent);
    static IControlsStack * spawnControlsStack (void);
};

//#endif // QT_OPENGL_LIB
#endif // QHASWIDGETS_H
