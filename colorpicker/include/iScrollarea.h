#ifndef ISCROLLAREA_H
#define ISCROLLAREA_H

/**
 * Scrolling control for a widget.
 */
class IScrollarea {
public:
    virtual void setXTranslation (int x) = 0;
    virtual void setYTranslation (int y) = 0;
    virtual int addXTranslation (int x) = 0;
    virtual int addYTranslation (int y) = 0;
    virtual void getTranslation (int & x, int & y) const = 0;

    virtual void setUniformScale (float scale) = 0;
    virtual float getUniformScale (void) const = 0;
    virtual void multiplyScale (float scale) = 0;

    virtual void setRotation (float angle) = 0;
    virtual void addRotation (float angle) = 0;
    virtual float getRotation (void) const = 0;
};

#endif // ISCROLLAREA_H
