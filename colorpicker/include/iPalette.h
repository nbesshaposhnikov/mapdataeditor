/***************************************************************************
 *   Copyright (C) 2009 by Anton R. <commanderkyle@gmail.com>              *
 *                                                                         *
 *   This file is a part of QAquarelle project.                            *
 *                                                                         *
 *   QAquarelle is free software; you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/
#ifndef IPALETTE_H
#define IPALETTE_H

#include "commdefs.h"
#include "commtypes.h"
#include "refcounter.h"

#include "iLogger.h"
#include "iSpawnedObject.h"
#include "iConnector.h"
#include "iPigmentMixture.h"
#include "iSkin.h"


enum ePaletteSortIndex {
    PALETTE_DONT_SORT           = 0,
    PALETTE_SORT_BY_HUE         = 1,
    PALETTE_SORT_BY_BRIGHTNESS  = 2
};

class IPalette : public IConnectorCallback {
public:

    virtual QWidget * asWidget (void) = 0;

    /**
     * Set the palette orientation.
     */
    virtual void setVertical (bool v) = 0;

    /**
     * Get the palette orientation.
     */
    virtual bool vertical (void) = 0;

    /**
     * Ask the palette window to hide
     * itself if it doesn't mind.
     */
    virtual void askToHide (void) = 0;

    /**
     * Set the columns count if the palette is
     * horizontal, or the rows count if it
     * is vertical.
     */
    virtual void setRowsCount (int len) = 0;

    /**
     * Get the row count.
     */
    virtual int rowsCount (void) = 0;

    /**
     * Add a pigment to palette.
     */
    virtual void addPigment (IPigmentMixture * pigment) = 0;

    /**
     * Remove all pigments from
     * the palette.
     */
    virtual void clear (void) = 0;

    /**
     * Load the palette from file.
     */
    virtual bool load (const char * path) = 0;

    /**
     * Save the palette to file.
     */
    virtual bool save (const char * path) = 0;

    /**
     * Sort the colors in palette.
     */
    virtual void sort (ePaletteSortIndex index, bool asc = TRUE) = 0;
};

IPalette * g_fac_SPAWN_PALETTE (QWidget * parent, ISkin * skin, bool isstatic);
IPalette * g_fac_SPAWN_PALETTE (void);

#endif // IPALETTE_H
