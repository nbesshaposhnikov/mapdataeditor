#ifndef IWORKSPACE_H
#define IWORKSPACE_H

#include "iLogger.h"
#include "iSpawnedObject.h"
#include "iVerboseObject.h"
#include "iDataContainer.h"
#include "iOpenGLHelper.h"
#include "iPaintingStyle.h"
#include "iCanvas.h"

#include <QWidget>

/**
 * Workspace descpriptor.
 */
class IWorkspace : public ISpawnedObject,
                   public IVerboseObject,
                   public IDataContainer {
public:

    /**
     * Get current canvas.
     */
    virtual ICanvas * canvas (void) = 0;

    /**
     * Get the current painting style.
     */
    virtual IPaintingStyle * paintingStyle (void) = 0;

    /**
     * Create the workspace data.
     * Must be called only if spawned without
     * specified dimentions.
     */
    virtual void create (long width, long height, int dpi) = 0;

    /**
     * Attach the paer switch widget to
     * enable the saving/loading its state along
     * with workspace data.
     */
    virtual void attachPaperSwitch (IPaperSwitch * p) = 0;
};

IWorkspace * g_fac_SPAWN_WORKSPACE (ICanvas * canvas, IOpenGLHelper * helper,
                                    long width = 0L, long height = 0L, int dpi = 100);

#endif // IWORKSPACE_H
