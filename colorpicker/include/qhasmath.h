#ifndef QHASMATH_H
#define QHASMATH_H

#include "commdefs.h"
#include "commtypes.h"
#include "refcounter.h"

#ifdef QT_OPENGL_LIB
#    include "GL/glew.h"
#endif

#include "iArray.h"
#include "iArrayFilter.h"

#include <QPoint>
#include <QList>

struct sRect {
    int left;
    int top;
    int right;
    int bottom;
};

/**
 * A point descriptor
 * in 3D space.
 */
class sPoint3f {
    float xyz[4] __attribute__((aligned(16)));

public:
    inline sPoint3f (void) {}
    inline sPoint3f (float x, float y, float z) {
        xyz[0] = x;
        xyz[1] = y;
        xyz[2] = z;
        xyz[3] = 0.0f;
    }

    float inline x (void) const {return xyz[0];}
    float inline y (void) const {return xyz[1];}
    float inline z (void) const {return xyz[2];}

    void inline setX (float x1) {xyz[0] = x1;}
    void inline setY (float y1) {xyz[1] = y1;}
    void inline setZ (float z1) {xyz[2] = z1;}

    float distanceTo  (const sPoint3f * target) const;
    float distance2To (const sPoint3f * target) const;

    /**
     * Check if the point is in the given triangle
     * in projection to y == 0.
     */
    bool inTriangle_xz (const sPoint3f * a, const sPoint3f * b, const sPoint3f * c) const;
};

/**
 * A point descriptor
 * in 2D space.
 */
class sPoint2f {
    float xyz[4] __attribute__((aligned(16)));

public:
    inline sPoint2f (void) {}
    inline sPoint2f (float x, float y) {
        xyz[0] = x;
        xyz[1] = y;
        xyz[2] = 0.0f;
        xyz[3] = 0.0f;
    }

    float inline x (void) const {return xyz[0];}
    float inline y (void) const {return xyz[1];}

    void inline setX (float x1) {xyz[0] = x1;}
    void inline setY (float y1) {xyz[1] = y1;}

    float distanceTo  (const sPoint2f * target) const;
    float distance2To (const sPoint2f * target) const;
};


/**
 * Spline definition.
 */
class ISpline {
public:
    virtual double f (double x) const = 0;
    virtual void release (void) = 0;

    /**
     * Replace the points and rebuild the spline.
     */
    virtual void rebuild (const double *x, const double *f, int nPoints) = 0;
    virtual void rebuild (QList <QPointF> &points) = 0;
};

class BSpline {
public:
    static void interpolate_quad (int   * x, int   * y, float t);
    static void interpolate_quad (float * x, float * y, float t);
};


class QHASMath {
public:

    /**
     * Allocate a Gaussian blur object.
     */
    static IArrayFilter * newGaussianBlur (void);

    /**
     * Create a spline from the given set of points.
     */
    static ISpline * newSpline (const double *x, const double *f, int nPoints);

    /**
     * Generate a gaussian random value
     * in [-1.0, +1.0].
     */
    static float randGaussian (void);
};

/**
 * Polygon-related methods.
 */
class QHASPolygon {
public:

    /**
     * Converts the polygon to convex
     * by removing some points.
     * @return New length of the list.
     */
    static int convexHull (sPoint3f ** list, int length);

    /**
     * Checks for intersection between two rectangles.
     * If dest is not NULL, it will be filled with the intersection result.
     */
    static bool intersects (const sRect * rect0, const sRect * rect1, sRect * dest = NULL);
};


void g_MAT3x3_identity  (float * m);
void g_MAT3x3_vmultiply (const float * v0, const float * m, float * v);
void g_MAT3x3_mmultiply (const float * m0, const float * m1, float * m);
void g_MAT3x3_invert    (const float * m0, float * m1);

/**
 * Return the angle in degrees of the point
 * in a cirle given by its coordinates.
 */
int g_angleFromCoords (float x, float y);

/**
 * Kubelka-Munk theory helper.
 */
class KubelkaMunk {
public:
    /**
     * Color declared as KM pair
     * seen over surface with given
     * reflectance.
     */
    static void colorOverBackground (
            const float * absorbing,
            const float * scattering,
            const float * reflectance,
                  float * rgb
    );

    /**
     * Color declared as KM pair
     * seen over white surface.
     */
    static void colorOverWhite (
            const float * absorbing,
            const float * scattering,
                  float * rgb
    );

    /**
     * Color declared as KM pair
     * seen over black surface.
     */
    static void colorOverBlack (
            const float * absorbing,
            const float * scattering,
                  float * rgb
    );
};

float atanh (float x);
float acoth (float x);
float smoothStep (float edge0, float edge1, float x);

#endif // QHASMATH_H
