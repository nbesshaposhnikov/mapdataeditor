/***************************************************************************
 *   Copyright (C) 2009 by Anton R. <commanderkyle@gmail.com>              *
 *                                                                         *
 *   This file is a part of QAquarelle project.                            *
 *                                                                         *
 *   QAquarelle is free software; you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/
#ifndef IPIGMENTMIXTURE_H
#define IPIGMENTMIXTURE_H


// Color component type.
typedef float TCC;

/**
 * The method of choosing the
 * color to paint with.
 */
enum eColorMetod {
    COLORMETHOD_KMSAMPLE,       /**< Choose the single color by a dialog box.   */
    COLORMETHOD_PALETTE         /**< Use the color palette to mix colors.       */
};

#include "iSpawnedObject.h"


/// Interface IPigmentMixture
class IPigmentMixture : public ISpawnedObject {

public:
    /**
     * Pigment name if any.
     */
    virtual const char * name (void) const = 0;

    virtual float granulation (void) const = 0;

    virtual float stainingPower (void) const = 0;

    virtual float weight (void) const = 0;

    /**
     * Returns the pointer to an RGB array
     * of absorbtion coefficients.
     */
    virtual const TCC * absorbing (void) const = 0;

    /**
     * Returns the pointer to an RGB array of
     * scattering coefficients.
     */
    virtual const TCC * scattering (void) const = 0;

    /**
     * Write the XML description into
     * the buffer and returns the number
     * of bytes written.
     */
    virtual long toXML (char * buffer, int capacity) const = 0;

    /**
     * Estimated color hue.
     */
    virtual int sortIndexHue (void) const = 0;

    /**
     * Estimated color brightness.
     */
    virtual int sortIndexBrightness (void) const = 0;

    /**
     * Check if the color description is valid.
     */
    virtual bool isValid (void) const = 0;
};

#endif //IPIGMENTMIXTURE_H
