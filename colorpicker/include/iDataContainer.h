#ifndef IDATACONTAINER_H
#define IDATACONTAINER_H

#include <QDataStream>


class IDataContainer {
public:

    /**
     * Check if the container's content
     * was changed since the last commit.
     */
    virtual bool changesCheck (void) const = 0;

    /**
     * Commit the changes. This resets the
     * result returned by changesCheck ().
     */
    virtual void changesCommit (void) = 0;

    /**
     * The length of buffer required
     * to save the cantainer's data.
     */
    virtual unsigned long requiredBufferSize (void) const = 0;

    /**
     * Save the container's data into
     * the external buffer.
     * @return Number of bytes actually written.
     */
    virtual unsigned long saveData (QDataStream * stream, unsigned long capacity) = 0;

    /**
     * Load the data from  the external
     * buffer into container.
     * @return Number of bytes actually read.
     */
    virtual unsigned long loadData (QDataStream * stream, unsigned long capacity) = 0;
};

#endif // IDATACONTAINER_H
