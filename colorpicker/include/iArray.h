/***************************************************************************
 *  Copyright (C) 2009 by Anton R. <commanderkyle@gmail.com>
 *
 *  This file is a part of QAquarelle project.
 *
 *  QAquarelle is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  QAquarelle is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with QAquarelle.  If not, see <http://www.gnu.org/licenses/>.
 */
#pragma once
#include "iOpenGLHelper.h"


#define ALIGN32(x) ((void*)(((DWORD)x + 32) & ~0x1f));


class IArray : public ISpawnedObject {
public:
    enum eFLAGS {
        FLAGS_NULL          = 0,
        FLAGS_NO_SHARE      = 1,
        FLAGS_MIPMAPPING    = 2
    };

    /**
     * Lock the array data for software access.
     * If data is currently loaded into some hardware,
     * it will be extracted back. You must call unlock ()
     * to let the data be returned to hardware if needed,
     */
    virtual void * lock_pointer (GLenum access = GL_READ_WRITE_ARB) = 0;

    /**
     * Lock the specified data region.
     * FIXME Actually, this call shouldn't be used for it will cause the random data be sent to GPU when unlocked.
     */
    virtual void * lock_pointer (int x, int y, int w, int h, GLenum access = GL_READ_WRITE_ARB) = 0;

    /**
     * Lock the array data for software read-only access.
     * If data is currently loaded into some hardware,
     * it will be extracted back. You must call unlock ()
     * to let the data be returned to hardware if needed,
     */
    virtual const void * lock_pointer_const (void) = 0;

    /**
     * Lock the specified data region.
     */
    virtual const void * lock_pointer_const (int x, int y, int w, int h) = 0;

    /**
     * Lock the array data for hardware access.
     * Returns the handle of data loaded into
     * the hardware. If the data is extracted now,
     * it will be rpacked back if not locked.
     * @return Handle of -1 if can't lock or not implemented.
     */
    virtual LONG lock_gltex (void) = 0;

    /**
     * Create a new object with the same
     * content.
     */
    virtual IArray * duplicate (void) = 0;

    /**
     * Unlock the array data
     * wherever it is located,
     */
    virtual void unlock (void) = 0;

    /**
     * The width of array.
     */
    virtual LONG width (void) const = 0;

    /**
     * The heigth of array.
     */
    virtual LONG height (void) const = 0;

    /**
     * Size of a single array element.
     */
    virtual LONG element_size (void) const = 0;

    /**
     * Replace the data in array by
     * providing the handle to new data
     * plane in hardware.
     * @param new_handle Handle to new texture.
     * @return Handle to old texture or -1 if failed.
     */
    virtual LONG swap (LONG new_handle) = 0;

    /**
     * Fill the array with zeros.
     */
    virtual void zero (void) = 0;

    /**
     * Fill the array with given data.
     */
    virtual void fill (BYTE r, BYTE g, BYTE b, BYTE a) = 0;

    /**
     * Fill the array with given data.
     */
    virtual void fill (float r, float g, float b, float a) = 0;

    /**
     * Move all buffer contents to software
     * memory and free all OpenGL-related stuff.
     */
    virtual void purgeToSW (void) = 0;

    /**
     * Format of the pixel data returned by lock_pointer ().
     */
    virtual GLenum transferFormat (void) const = 0;

    /**
     * Type of the pixel data returned by lock_pointer ().
     */
    virtual GLenum transferType (void) const = 0;
};

#ifdef QT_OPENGL_LIB

IArray * g_fac_SPAWN_ARRAY (IOpenGLHelper * helper, LONG width, LONG height,
                            GLenum format, GLenum type, int flags);

#endif
