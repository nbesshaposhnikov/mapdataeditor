/***************************************************************************
 *   Copyright (C) 2009 by Anton R. <commanderkyle@gmail.com>              *
 *                                                                         *
 *   This file is a part of QAquarelle project.                            *
 *                                                                         *
 *   QAquarelle is free software; you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/
#ifndef IPIPELINE_H
#define IPIPELINE_H

#include "iPlugin.h"
#include "iVerboseObject.h"
#include "iChannel.h"

enum ePIPELINE_PRIORITY {
    PIPELINE_PRIORITY_LOW    = 0,
    PIPELINE_PRIORITY_NORMAL = 1,
    PIPELINE_PRIORITY_HIGH   = 2
};

/**
 * A pipeline class encapsulates the
 * processing to be done on a channel data.
 */
class IPipeline : public IPlugin,
                  public IVerboseObject {

public:

    /**
     * Check if the pipeline accepts the
     * given channel type.
     */
    virtual bool acceptsChannel (FOURCC uid) = 0;

    /**
     * Add the given channel to the processing queue.
     * The channel is automatically detached after
     * each iterate () call.
     * /return TRUE if the channel has been accepted.
     */
    virtual bool pushChannel (IChannel * channel) = 0;

    /**
     * Pipeline priority controls the position
     * of a given processor in a processing queue.
     */
    virtual int priority (void) = 0;

    /**
     * Run a single iteration on all attached channels.
     * /return TRUE if any channel has been changed.
     */
    virtual bool iterate (float dt) = 0;

    /**
     * Reload all OpenGL shaders.
     */
    virtual void reloadShaders (void) = 0;
};

/**
 * A pipeline factory.
 */
class IPipelineFactory : public IPlugin {
public:
    /**
     * Get the number of pipelines
     * in this library.
     */
    virtual int pipelinesInLibrary (void) = 0;

    /**
     * Attach the logger object that will
     * be used to log the messages for
     * all allocated pipelines.
     */
    virtual void attachLogger (ILogger * logger) = 0;

    /**
     * Create a pipeline.
     * @param index Pipeline index, bounded by the value returned by pipelinesInLibrary ().
     * @param helper OpenGL helper object.
     */
    virtual IPipeline * create (int index, IOpenGLHelper * helper) = 0;
};

#ifdef ALLOW_INTERFACE_DECLARATION
Q_DECLARE_INTERFACE (IPipeline,          "org.BlurredVision.QASuite.IPipeline/1.0");
Q_DECLARE_INTERFACE (IPipelineFactory,   "org.BlurredVision.QASuite.IPipelineFactory/1.0");
#endif

#endif //IPIPELINE_H
