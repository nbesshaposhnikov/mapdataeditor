/***************************************************************************
 *   Copyright (C) 2009 by Anton R. <commanderkyle@gmail.com>              *
 *                                                                         *
 *   This file is a part of QAquarelle project.                            *
 *                                                                         *
 *   QAquarelle is free software; you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/
#ifndef LOGGER_SIGNAL_H
#define LOGGER_SIGNAL_H

#include "commtypes.h"
#include "commdefs.h"
#include "refcounter.h"
#include "iLogger.h"

#include <QObject>
#include <QFile>
#include <QDateTime>

class Logger_signal : public QObject,
                      public ILogger,
                      public CRefCounter {

    Q_OBJECT;

    int  m_Id;
    char m_Buffer[1024];

    virtual void suicide (void) {
        delete this;
    }

signals:
    void message (eLOG_IMPORTANCE i, const char * str);

public:
    Logger_signal (void) {
        m_Id = ISpawnedObject::allocateId ();
    }

    /**
     * Get the Id of current allocated object.
     */
    virtual int objectId (void) {
        return m_Id;
    }

    /**
     * Get the classname for this object.
     */
    virtual const char * myClassName (void) {
        return "Client::Logger_signal";
    }

    /**
     * Increase the object's
     * reference counter.
     */
    virtual void addRef (void) {
        CRefCounter::addRef ();
    }

    /**
     * Release the object.
     */
    virtual void release (void) {
        CRefCounter::release ();
    }

    /**
     * Ask the object to emit all
     * notification signals if any.
     */
    virtual void pokeObject (void) {
    }

    /**
     * Write the log entry for a specific object.
     */
    virtual void write (
            eLOG_IMPORTANCE importance,
            ISpawnedObject * caller,
            const char * str, ...
    ) {
        long written = 0;
        QString time = QDateTime::currentDateTime().toString();
        if (caller) {
            written += snprintf (m_Buffer + written, 1024, "%s\t%s: ", (const char*)time.toLocal8Bit(), caller->myClassName ());
        } else {
            written += snprintf (m_Buffer + written, 1024, "%s\tGod: ", (const char*)time.toLocal8Bit());
        }

        va_list   va;
        va_start (va, str);
        written += vsnprintf (m_Buffer + written, 1024 - written, str, va);
        written += snprintf  (m_Buffer + written, 1024 - written, "\n");
        va_end (va);

        emit message (importance, m_Buffer);
    }

    /**
     * Write the log entry for a specific object.
     */
    virtual void write (
            eLOG_IMPORTANCE importance,
            ISpawnedObject * caller,
            const char * str,
            va_list val
    ) {
        long written = 0;
        QString time = QDateTime::currentDateTime().toString();
        if (caller) {
            written += snprintf (m_Buffer + written, 1024, "%s\t%s: ", (const char*)time.toLocal8Bit(), caller->myClassName ());
        } else {
            written += snprintf (m_Buffer + written, 1024, "%s\tGod: ", (const char*)time.toLocal8Bit());
        }

        written += vsnprintf (m_Buffer + written, 1024 - written, str, val);
        written += snprintf  (m_Buffer + written, 1024 - written, "\n");

        emit message (importance, m_Buffer);
    }
};

class Logfile_helper : public QObject {
    Q_OBJECT

    QFile * m_LogFile;
public:
    Logfile_helper (QFile * file) {
        m_LogFile = file;
    }

public slots:

    void write (eLOG_IMPORTANCE importance, const char * str) {
        Q_UNUSED (importance);

        // Duplicate everything into file if any,
        if (m_LogFile) {
            m_LogFile->write (str);
            m_LogFile->flush ();
        }
    }
};

#endif // LOGGER_SIGNAL_H
