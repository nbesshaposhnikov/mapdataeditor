/***************************************************************************
 *   Copyright (C) 2009 by Anton R. <commanderkyle@gmail.com>
 *
 *  This file is a part of QAquarelle project.
 *
 *  QAquarelle is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  QAquarelle is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with QAquarelle.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef IINSTRUMENT_H
#define IINSTRUMENT_H

#include "iPlugin.h"
#include "iInstrumentLock.h"
#include "iPigmentMixture.h"
#include "iDataContainer.h"
#include "iChannel.h"
#include "iLogger.h"
#include "iOpenGLHelper.h"


/**
 * Possible sources of input events.
 */
enum eInputMethod {
    INPUT_CONTROLLER_MOUSE,
    INPUT_CONTROLLER_TABLET,
    INPUT_CONTROLLER_GOD
};

/**
 * Predefined set of instrument
 * groups to enable their compact
 * displaying.
 */
enum eInstrumentGroup {
    INSTRUMENTGROUP_BRUSH,
    INSTRUMENTGROUP_PENCIL,
    INSTRUMENTGROUP_ERASER,
    INSTRUMENTGROUP_TOOLBOX,        /**< For some tool stuff that don't paint at all. =)    */
    INSTRUMENTGROUP_UNGROUPED
};

/// Interface to an instrument plugin that is used
/// to model the custom brush dynamics and return
/// it as the per-channel writing primitives.
class IInstrument : public IPlugin,
                    public IDataContainer {
public:

    /**
     * Apply the instrument.
     * The delegate class provides the methods
     * for interactng with a canvas.
     */
    virtual void applyInstrument (IChannel * channel, ICanvasDelegate * delegate) = 0;

    /**
     * Generate a copy of this instrument.
     */
    virtual IInstrument * replicate (void) = 0;

    /**
     * Change theinstrument position.
     * The coordinates are given in pixels
     * is canvas space.
     */
    virtual void setPosition (int x, int y, eInputMethod source) = 0;

    /**
     * Change the brush pressure.
     */
    virtual void setPressure (float pressure, eInputMethod source) = 0;

    /**
     * Change the brush tilt.
     */
    virtual void setTilt (float xtilt, float ytilt, eInputMethod source) = 0;

    /**
     * Get the width of current
     * writing primitive.
     */
    virtual int currentPrimitiveWidth (void) = 0;

    /**
     * Get the height of current
     * writing primitive.
     */
    virtual int currentPrimitiveHeight (void) = 0;

    /**
     * Absorbing component of current
     * color mixture or its average if the
     * single value is not available.s
     */
    virtual const float * currentAbsorbing (void) = 0;

    /**
     * Scattering component of current
     * color mixture or its average if the
     * single value is not available.s
     */
    virtual const float * currentScattering (void) = 0;

    /**
     * Returns a picture to display in a list
     * (currently it is QImage casted to (void *)).
     */
    virtual const void * image (void) = 0;

    /**
     * Returns the group this instrument
     * belongs to.
     */
    virtual eInstrumentGroup instrumentGroup (void) = 0;

    /**
     * Get the promoted channels by index.
     * Promoted channels are used to request the canvas
     * to ensure that it contains all nessesary channels
     * when the instrument is to be applied.
     * The plugin should use it to promote all channels
     * whose presence it considers important.
     * /return Channel uid or NULL if the index is out of range
     */
    virtual FOURCC enumPromotedChannels (int index) = 0;

    /**
     * Get the preferred color chooser for
     * this instrument.
     */
    virtual eColorMetod promoteColorChooser (void) const = 0;

    /**
     * Add the given pigment mixture
     * to the instrument.
     */
    virtual void addMass (IPigmentMixture * pigment, float amount) = 0;

    /**
     * Removes all components
     * from the instrument.
     */
    virtual void cleanMass (void) = 0;

    /**
     * Save the current brush geometrical
     * state in a stack.
     */
    virtual void statePush (void) = 0;

    /**
     * Restore brush state from a stack.
     */
    virtual void statePop (void) = 0;
};

class IInstrumentFactory : public IPlugin {
public:
    /**
     * Get the number of instruments
     * in this library.
     */
    virtual int instrumentsInLibrary (void) = 0;

    /**
     * Get the classname of nth instrument
     * in library.
     */
    virtual const char * instrumentClassname (int index) = 0;

    /**
     * Attach the logger object that will
     * be used to log the messages for
     * all allocated instruments.
     */
    virtual void attachLogger (ILogger * logger) = 0;

    /**
     * Create an instrument by index.
     * @param index Instrument index, bounded by the value returned by instrumentsInLibrary ().
     * @param helper OpenGL helper object.
     */
    virtual IInstrument * create (int index, IOpenGLHelper * helper) = 0;

    /**
     * Create an instrument by classname.
     * @param index Instrument index, bounded by the value returned by instrumentsInLibrary ().
     * @param helper OpenGL helper object.
     */
    virtual IInstrument * create (const char * name, IOpenGLHelper * helper) = 0;
};

#ifdef ALLOW_INTERFACE_DECLARATION
Q_DECLARE_INTERFACE (IInstrument,        "org.FreePainters.qAquarelle.IInstrument/1.0");
Q_DECLARE_INTERFACE (IInstrumentFactory, "org.FreePainters.qAquarelle.IInstrumentFactory/1.0");
#endif

#endif //IINSTRUMENT_H

