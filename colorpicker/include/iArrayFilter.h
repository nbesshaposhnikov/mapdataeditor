#ifndef IARRAYFILTER_H
#define IARRAYFILTER_H

#include "iSpawnedObject.h"
#include "iArray.h"

class IArrayFilter : public ISpawnedObject {
public:

    /**
     * Filer the given target with
     * kernel of specified radius.
     * @param src Source/destination image.
     * @param tmp Temporary image buffer.
     * @param r Kernel radius in pixels.
     */
    virtual void filter (IArray * src, IArray * tmp, float r) = 0;
};

#endif // IARRAYFILTER_H
