/***************************************************************************
 *  Copyright (C) 2009 by Anton R. <commanderkyle@gmail.com>
 *
 *  This file is a part of QAquarelle project.
 *
 *  QAquarelle is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  QAquarelle is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with QAquarelle.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef IIMAGETRANSACTION_H
#define IIMAGETRANSACTION_H


/**
 * A single transaction.
 */
class ITransaction {
public:
    /**
     * Add an image patch to current transaction.
     * @param patch Image patch to add.
     * @param channel Unique id of a channel this patch belongs to.
     * @param patch_x X-position of this patch in a grid.
     * @param patch_y Y-position of this patch in a grid.
     */
    virtual void addImagePatch (IArray * patch, unsigned long channel, int patch_x, int patch_y) = 0;

    /**
     * Commit the transaction and
     * free this object.
     */
    virtual void commit (void) = 0;
};

/**
 * ImageTransaction base interface.
 * This interface is used to control
 * the actions stack.
 */
class IImageTransaction {
public:

    /**
     * Begin a new transaction.
     */
    virtual ITransaction * beginTransaction (void) = 0;

    /**
     * Rollback the N last transactions.
     */
    virtual void rollback (int depth) = 0;
};

#endif // IIMAGETRANSACTION_H
