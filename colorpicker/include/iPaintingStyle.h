#ifndef IPAINTINGSTYLE_H
#define IPAINTINGSTYLE_H


/**
 * Different presets for a
 * painting style.
 */
enum ePAINTINGSTYLE_PRESET {
    PAINTINGSTYLE_CUSTOM       = -1,
    PAINTINGSTYLE_PLAIN        = 0,     /**< For non-paint sources such as pencil.  */
    PAINTINGSTYLE_WATERCOLOURS = 1,     /**< For Watercolours.                      */
    PAINTINGSTYLE_ACRILYCS     = 2,     /**< For Acrylics.                          */
    PAINTINGSTYLE_OIL          = 3,     /**< For Oils.                              */
    PAINTINGSTYLE_INK          = 4,     /**< For Ink.                               */
    PAINTINGSTYLES_COUNT       = 5,     /**< Enumerator length.                     */
};

/**
 * Describes the paint type
 * hence controlling some
 * painting parameters.
 */
class IPaintingStyle {
public:

    /**
     * Set the painting style parameters
     * from the given preset.
     */
    virtual void fromPreset (ePAINTINGSTYLE_PRESET preset) = 0;

    /**
     * Promoted paint drying speed.
     */
    virtual float paintDryingSpeed (void) const = 0;

    /**
     * Promoted paint viscosity.
     */
    virtual float paintViscosity (void) const = 0;

    /**
     * Promoted paint opacity.
     */
    virtual float paintOpacity (void) const = 0;

    /**
     * Promoted paint staining power.
     */
    virtual float paintStainingPower (void) const = 0;

    virtual void setDryingSpeed(float) = 0;
    virtual void setViscosity(float) = 0;
    virtual void setOpacity(float) = 0;
    virtual void setStainingPower(float) = 0;

    virtual ePAINTINGSTYLE_PRESET closestPreset (void) const = 0;
};

#endif // IPAINTINGSTYLE_H
