/***************************************************************************
 *   Copyright (C) 2009 by Anton R. <commanderkyle@gmail.com>              *
 *                                                                         *
 *   This file is a part of QAquarelle project.                            *
 *                                                                         *
 *   QAquarelle is free software; you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/
#include "commdefs.h"
#include "commtypes.h"
#include "refcounter.h"
#include "iSpawnedObject.h"


/**
 * Static object counter. O_o
 */
static int g_ObjectCounter   = 0;
static int g_ObjectCounter_1 = 0;

static ISpawnedObject * g_AllocatedObjects = NULL;


ISpawnedObject * ISpawnedObject::orphans (void) {
    return g_AllocatedObjects;
}

int ISpawnedObject::totalObjects (void) {
    return g_ObjectCounter_1;
}

int ISpawnedObject::allocateId (void) {
    return g_ObjectCounter++;
}

void ISpawnedObject::_register (ISpawnedObject * me) {
    me->linkTo    (g_AllocatedObjects);

    g_AllocatedObjects = me;
    g_ObjectCounter_1++;
}

void ISpawnedObject::_unregister (ISpawnedObject * me) {
    ISpawnedObject * prev = NULL;
    for (ISpawnedObject * p = g_AllocatedObjects; p; p = p->next ()) {
        if (p == me) {
            if (prev) {
                prev->linkTo (me->next ());
            } else {
                g_AllocatedObjects = me->next ();
            }

            g_ObjectCounter_1--;
            me->linkTo (NULL);
            return;
        }

        prev = p;
    }

    if (prev) {
        prev->linkTo (me->next ());
    }
}
