/***************************************************************************
 *  Copyright (C) 2009 by Anton R. <commanderkyle@gmail.com>
 *
 *  This file is a part of QAquarelle project.
 *
 *  QAquarelle is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  QAquarelle is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with QAquarelle.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef ICHANNEL_H
#define ICHANNEL_H

#include "iInstrumentLockEx.h"
#include "iOpenGLHelper.h"
#include "iArrayGrid.h"
#include "iDataContainer.h"


static FOURCC inline g_MAKE_FOURCC (char a, char b, char c, char d) {
    return (FOURCC)((a << 24) | (b << 16) | (c << 8) | d);
}

#define CHANNELUID_Optical              g_MAKE_FOURCC ('K', 'S', 'R', 'T')
#define CHANNELUID_Meta0                g_MAKE_FOURCC ('M', 'E', 'T', '0')
#define FOURCC_NULL                     g_MAKE_FOURCC (0, 0, 0, 0)

/***
 * Optical channel:
 *
 * -------------
 *  Fg-K | Fg-S
 * -------------
 *  Bg-R | Bg-T
 * -------------
 *
 * Meta0 channel:
 *
 * -------------
 *   E4  | Bump
 * -------------
 *   -   |  -
 * -------------
 */


/**
 * A factory call.
 */
IChannel * g_fac_SPAWN_CHANNEL (FOURCC uid, int width, int height, IOpenGLHelper * helper);

struct sTouch {
    int   x;                /**< Touched x position.    */
    int   y;                /**< Touched y position.    */
    int   stroke_length;    /**< Current stroke length. */
    bool  feedback;         /**< Feedback trigger.      */
};

/// Interface IChannel
/// Interface to a data channel.
class IChannel : public IDataContainer,
                 public ISpawnedObject {
public:
    static const char * nameFromUID (FOURCC uid);

    virtual void addRef (void) = 0;

    virtual void release (void) = 0;

    /**
     *  Channel type identifier.
     */
    virtual FOURCC uid (void) = 0;

    /**
     * Get the position of this channel
     * in a channel stack.
     */
    virtual int stackPosition (void) = 0;

    /**
     * Apply the given instrument to the
     * channel data at the given positions.
     */
    virtual void touch (IInstrumentLockEx * operation,
                        const sTouch * stroke,
                        int length) = 0;

    /**
     * Remove all data fromm channel.
     */
    virtual void clear (void) = 0;

    /**
     * Lock the contents
     * of this channel.
     */
    virtual IArrayGrid * lock (void) = 0;

    /**
     * Unlock he channel.
     */
    virtual void unlock (void) = 0;

    /**
     * Get the width of a channel buffer.
     */
    virtual int width (void) = 0;

    /**
     * Get the height of the channelbuffer.
     */
    virtual int height (void) = 0;

    /**
     * Resize the channel.
     */
    virtual void resize (int width, int height) = 0;

    /**
     * Set the layer dpi for paper texture scaling.
     */
    virtual void setLayerDpi (int dpi) = 0;

    /**
     * Reload all OpenGL shaders.
     */
    virtual void reloadShaders (void) = 0;
};

#endif //ICHANNEL_H

