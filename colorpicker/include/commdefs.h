/***************************************************************************
 *   Common include file.                                                  *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/
#pragma once

#ifndef TRUE
#   define TRUE               1
#   define FALSE              0
#endif

#ifndef NULL
#   define NULL     0
#endif

#define SAFE_DELETE(x)    {if (x) {delete x; x = 0;}}
#define SAFE_DELETE_A(x)  {if (x) {delete [] x; x = 0;}}
#define SAFE_FREE(x)      {if (x) {free (x); x = 0;}}
#define SAFE_RELEASE(x)   {if (x) {x->release (); x = 0;}}

template<class T> inline T MAX (T a, T b) {
    return a >= b ? a : b;
}

template<class T> inline T MIN (T a, T b) {
    return a < b ? a : b;
}

template<class T> inline void SWAP (T &a, T &b) {
    T t;
    t = a;
    a = b;
    b = t;
}

template<class T> inline bool IS_SIGNIFICANT (T x) {
    return x > 0.001f;
}

#undef SPAWN
#undef NEW
#undef DELETE
#define SPAWN(x)           new x
#define NEW(x)             new x
#define DELETE(x)          delete x


#undef DATA_PATH
#if WIN32 || !INSTALLATION
#   define DATA_PATH QApplication::applicationDirPath ()
#else
#   define DATA_PATH QString("/usr/share/qaquarelle/")
#endif

#ifndef DEBUG
#   define PROFILE_FUNC()
#else
#   ifndef NO_SHINY_PLEASE
#       include "Shiny.h"
#   endif
#endif
