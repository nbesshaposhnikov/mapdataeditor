#ifndef MAPLISTMODEL_H
#define MAPLISTMODEL_H

#include <QAbstractListModel>
#include "mapdata.h"

class MapListModel : public QAbstractListModel
{
    Q_OBJECT
public:
    explicit MapListModel(QObject *parent = 0);
    /**
     * Returns the number of rows.
     */
    int rowCount(const QModelIndex &parent = QModelIndex()) const;

    /**
     * Returns the data stored under the given <i>role</i> for the item
     * referred to by the <i>index</i>.
     */
    QVariant data(const QModelIndex &index,
                  int role = Qt::DisplayRole) const;

    bool setData ( const QModelIndex & index, const QVariant & value, int role = Qt::EditRole );

    /**
     * Makes sure the items are checkable.
     */
    Qt::ItemFlags flags(const QModelIndex &index) const;

    /**
     * Returns the headers for the table.
     */
    QVariant headerData(int section, Qt::Orientation orientation,
                        int role = Qt::DisplayRole) const;

    void setMapList(MapDataList *newMapList);

private:
    MapDataList *mapList;
signals:
    
public slots:
    
};

#endif // MAPLISTMODEL_H
