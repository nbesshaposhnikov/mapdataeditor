#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QtGui>
#include "mapdata.h"
#include "maplistmodel.h"
#include "regionlistmodel.h"
#include "editdialog.h"
#include <QtWidgets/QMainWindow>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT
    
public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();
    
private:
    Ui::MainWindow *ui;
    MapDataList mapDataList;
    MapListModel *mapListModel;
    RegionListModel *regionListModel;
    QString fileName;
private slots:
    void onMapChange(QModelIndex index);
    void onAddMap();
    void onDeleteMap();
    void onAddRegion();
    void onDeleteRegion();
    void onEditRegion();
    void onOpen();
    void onSave();
    void onSaveAs();
};

#endif // MAINWINDOW_H
