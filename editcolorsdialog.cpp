#include "editcolorsdialog.h"
#include "ui_editcolorsdialog.h"


EditColorsDialog::EditColorsDialog(const MapData &m,QWidget *parent) :
    QDialog(parent),
    ui(new Ui::EditColorsDialog),
    mapData(m)
{
    ui->setupUi(this);

    colorPicker = new ColorPicker(this);
    ui->colorLayout->addWidget(colorPicker);

    for(int i=0;i<mapData.regionList.size();++i) {
        ui->regionList->addItem(mapData.regionList[i].name);
    }
    onRegionChange(ui->regionList->currentIndex());

    connect(ui->regionList,SIGNAL(currentIndexChanged(int)),this,SLOT(onRegionChange(int)));
    connect(colorPicker,SIGNAL(colorChanged(QColor)),this,SLOT(onColorChange(QColor)));

    connect(ui->redEdit,SIGNAL(valueChanged(int)),this,SLOT(onColorValueChanged(int)));
    connect(ui->greenEdit,SIGNAL(valueChanged(int)),this,SLOT(onColorValueChanged(int)));
    connect(ui->blueEdit,SIGNAL(valueChanged(int)),this,SLOT(onColorValueChanged(int)));

    ui->redEdit->setValue(colorPicker->color().red());
    ui->greenEdit->setValue(colorPicker->color().green());
    ui->blueEdit->setValue(colorPicker->color().blue());
}

EditColorsDialog::~EditColorsDialog()
{
    delete ui;
}

void EditColorsDialog::onColorChange(QColor color)
{
    int row = ui->regionList->currentIndex();

    mapData.regionList[row].dwRGB = color.rgb() & 0x00ffffff;

    ui->redEdit->setValue(color.red());
    ui->greenEdit->setValue(color.green());
    ui->blueEdit->setValue(color.blue());
}

void EditColorsDialog::onRegionChange(int index)
{
    colorPicker->setColor(QColor(mapData.regionList[index].dwRGB));
}

void EditColorsDialog::onColorValueChanged(int)
{
    QColor color;

    color.setRed(ui->redEdit->value());
    color.setGreen(ui->greenEdit->value());
    color.setBlue(ui->blueEdit->value());

    colorPicker->setColor(color);
}

