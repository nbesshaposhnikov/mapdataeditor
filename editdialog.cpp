﻿#include "editdialog.h"
#include "ui_editdialog.h"

EditDialog::EditDialog(const MapData &d,QWidget *parent) :
    QDialog(parent),
    data(d),
    ui(new Ui::EditDialog),
    regionImage(QPixmap()),
    colorLayout(0)
{
    ui->setupUi(this);
    setLayout(ui->mainLayout);

    loadPixmap();
    connect(ui->import_2,SIGNAL(clicked()),this,SLOT(onImport()));
    connect(ui->export_2,SIGNAL(clicked()),this,SLOT(onExport()));
    connect(ui->edit,SIGNAL(clicked()),this,SLOT(onEdit()));
}

EditDialog::~EditDialog()
{
    delete ui;
}

void EditDialog::loadPixmap(QImage image)
{
    if(image==QImage()) {
        image = QImage(QSize(data.width,data.height),QImage::Format_RGB32);

        for(int y=0;y<data.height;++y) {
            for(int x=0;x<data.width;++x) {
                int i=data.data[data.width*y+x];
                if(data.data[data.width*y+x] == data.regionList.size())
                    image.setPixel(x,y,qRgb(255,255,255));
                else
                    image.setPixel(x,y,data.regionList[i].dwRGB );
            }
        }

    }

    QPixmap pixmap = QPixmap::fromImage(image);
    regionImage = pixmap;

    scene = new QGraphicsScene(this);
    scene->clear();
    scene->addPixmap(pixmap);

    ui->graphicsView->setScene(scene);
    ui->graphicsView->setTransform(QTransform::fromScale(2.0,2.0));

    if(colorLayout)
        delete colorLayout;
    colorLayout = new QVBoxLayout;
    QHBoxLayout *hbox;

    for(int i=0;i<data.regionList.size();++i) {
        hbox = new QHBoxLayout;
        QLabel *l1 = new QLabel((data.regionList[i].name));
        QLabel *l2 = new QLabel((data.regionList[i].name));
        pixmap = QPixmap(20,20);

        pixmap.fill(QColor(data.regionList[i].dwRGB));
        l1->setPixmap(pixmap);

        hbox->addWidget(l1);
        hbox->addWidget(l2);
        colorLayout->addLayout(hbox);
    }
    ui->colorList->setLayout(colorLayout);
}

void EditDialog::onImport()
{
    QString fileName = QFileDialog::getOpenFileName(this,tr("Открыть"),QString(),"BMP (*.bmp)");
    regionImage.load(fileName,"BMP");

    QImage image = regionImage.toImage();

    data.width = image.width();
    data.height = image.height();

    if(data.data)
        delete data.data;

    data.data = new unsigned char[data.width*data.height];
    for(int y=0;y<data.height;++y) {
        for(int x=0;x<data.width;++x) {
            int i = data.findColor(image.pixel(x,y)&0x00ffffff);
            data.data[data.width*y+x] = i;
        }
    }
    loadPixmap(image);
}

void EditDialog::onExport()
{
    QString fileName = QFileDialog::getSaveFileName(this,tr("Сохранить"),QString(),"BMP (*.bmp)");

    if(!fileName.isEmpty()) {
        regionImage.save(fileName,"BMP",100);
    }
}

void EditDialog::onEdit()
{
    EditColorsDialog *d = new EditColorsDialog(data,this);

    if(d->exec()) {
        data = d->getMapData();
        loadPixmap();
    }

    delete d;
}
