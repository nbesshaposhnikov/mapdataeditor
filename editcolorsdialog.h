#ifndef EDITCOLORSDIALOG_H
#define EDITCOLORSDIALOG_H

#include <QtGui>
#include "mapdata.h"
#include <QPixmap>
#include "colorpicker/colorpicker.h"
#include <QtWidgets/QDialog>

namespace Ui {
class EditColorsDialog;
}

class EditColorsDialog : public QDialog
{
    Q_OBJECT
    
public:
    explicit EditColorsDialog(const MapData &m,QWidget *parent = 0);
    ~EditColorsDialog();
    MapData &getMapData() {
        return mapData;
    }

private:
    Ui::EditColorsDialog *ui;
    MapData mapData;

    ColorPicker *colorPicker;
public slots:
    void onColorChange(QColor color);
    void onRegionChange(int index);
    void onColorValueChanged(int color);
};

#endif // EDITCOLORSDIALOG_H
