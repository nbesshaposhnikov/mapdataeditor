#ifndef REGIONLISTMODEL_H
#define REGIONLISTMODEL_H

#include <QAbstractTableModel>
#include "mapdata.h"


class RegionListModel : public QAbstractTableModel
{
    Q_OBJECT
public:
    explicit RegionListModel(QObject *parent = 0);

    void setMapData(MapData* newMapData);
private:
    MapData* mapData;

    int rowCount ( const QModelIndex & parent = QModelIndex() ) const;
    int columnCount ( const QModelIndex & parent = QModelIndex() ) const;
    QVariant data ( const QModelIndex & index, int role = Qt::DisplayRole ) const;
    QVariant headerData ( int section, Qt::Orientation orientation, int role = Qt::DisplayRole ) const;
    bool setData ( const QModelIndex & index, const QVariant & value, int role = Qt::EditRole );
    Qt::ItemFlags flags ( const QModelIndex & index ) const;
    bool insertRows ( int row, int count, const QModelIndex & parent = QModelIndex() );
    bool removeRows ( int row, int count, const QModelIndex & parent = QModelIndex() );

    //Get Column data
    QVariant getColumnData( const QModelIndex & index) const;
    //Set
    bool setColumnData(const QModelIndex & index,const QVariant & value);
signals:
    
public slots:
    
};

#endif // REGIONLISTMODEL_H
